#include <cstdio>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cfloat>

#include "deformable.h"
#include "fluidsim.h"

using namespace std;

fptype timestep = 1.0 / 60;
fptype g[] = {0, -1, 0};

Deformable* def;
fptype defMass;
Fluid fluid;

void exportSurface(string path, int nn, int nb, int* boundary) {
   ofstream outfile(path.c_str());
   outfile << nn << " " << nb << endl;
   for(int i = 0; i < nb; i++) {
      outfile << boundary[3 * i] << " " << boundary[3 * i + 1] << " "
         << boundary[3 * i + 2] << endl;
   }
   outfile.close();
}

void exportFrame(string path, int frame, const VectorXfp& nodes,
   const vector<Vec3fp>& particles, fptype radius) {
   stringstream strout;
   strout << path << "frame_" << frame << ".txt";
   string filepath = strout.str();
   
   ofstream outfile(filepath.c_str());
   // Write vertex count and particle radius.
   outfile << particles.size() << " " << radius << endl;
   // Write vertices.
   for(int i = 0; i < particles.size(); i++) {
      outfile << particles[i][0] << " " << particles[i][1] << " "
         << particles[i][2] << endl;
   }
   
   // Write nodes.
   for(int i = 0; i < nodes.size() / 3; i++) {
      outfile << nodes[3 * i] << " " << nodes[3 * i + 1] << " "
         << nodes[3 * i + 2] << endl;
   }
   
   outfile.close();
}

int main(int argc, char **argv)
{
   omp_set_num_threads(4);
   
   // Solid material parameters.
   fptype rho = 5;           // Density.
   fptype mu = 50;            // Second Lame parameter.
   fptype lambda = mu;  // First Lame parameter.
   fptype damping_m = 0;      // Mass damping coefficient.
   fptype damping_s = 0.025;  // Stiffness damping coefficient.
   
   // Fluid parameters.
   int N = 102;                // Grid resolution.
   fptype dx = 1.0 / N;       // Grid cell size.
   fptype rad = dx * 1.01 * sqrt(3.0) / 2.0;
      //2.5 / N / M;  // Particle radius.
   
   // Solid shift amounts.
   fptype sx = 0.5;
   fptype sy = 0.5;
   fptype sz = 0.5;
   fptype radius = 0.3;
   // Set up the solid.
   def = new CorotatedLinearDeformable();
   def->createFromFile("hollow_fixed.fm", radius);
   def->build(rho, mu, lambda, damping_m, damping_s);
   def->shift(sx, sy, sz);
   defMass = def->getVolume() * rho;
   
   // Set up the fluid.
   fluid.initialize(25, N, rad);
   fluid.setProjectionMethod(PPM_FORCE_JACOBIAN_SPD);
   fluid.setIterativeSolve(true);
   fluid.setUseCFL(true);
   fluid.setGravity(g[0], g[1], g[2]);
   fluid.setDeformable(def);
   fluid.setInfoStream("./timing_info.txt");
   
   /*fptype angle = M_PI / 4;
   fptype alpha = 0.3;
   fptype n_p = 128 * alpha * radius * radius * radius / (dx * dx * dx);
   fptype cosa = cos(angle);
   fptype sina = sin(angle);
   Vec3fp centre(sx, sy, sz);
   for(int i = 0; i < n_p; i++) {
      fptype x = randhashd(3 * i) * radius * alpha - radius;
      fptype y = (2 * randhashd(3 * i + 1) - 1) * radius;
      fptype z = (2 * randhashd(3 * i + 2) - 1) * radius;
      Vec3fp p(x, y, z);
      if(mag(p) < radius) {
         p[0] = x * cosa - y * sina;
         p[1] = x * sina + y * cosa;
         fluid.addParticle(centre + p);
         //p[0] = -p[0];
         //fluid.addParticle(centre + p);
      }
   }*/
   fptype inner_r = radius / 3;
   fptype n_p = 80 * inner_r * inner_r * inner_r / (dx * dx * dx);
   Vec3fp centre(sx, sy, sz);
   for(int i = 0; i < n_p; i++) {
      fptype x = randhashd(3 * i) * 2 * inner_r - inner_r;
      fptype y = randhashd(3 * i + 1) * 2 * inner_r - inner_r;
      fptype z = randhashd(3 * i + 2) * 2 * inner_r - inner_r;
      Vec3fp p(x, y, z);
      if(mag(p) < inner_r) {
         fluid.addParticle(centre + p);
      }
   }
   
   def->setBounds(dx, 1 - dx, dx, 1 - dx, dx, 1 - dx);
   
   // Apply some acceleration.
   fptype a[] = {1, 0, 0};
   fluid.addAcceleration(a, 1);
   
   exportSurface("odata/surface.txt", def->n_v, def->n_b, def->boundary);
   exportFrame("odata/", 0, def->xs, fluid.particles, rad);
   for(int frame = 1; frame <= 600; frame++) {
      fluid.advance(timestep);
      exportFrame("odata/", frame, def->xs, fluid.particles, rad);
      cout << "Finished with frame " << frame << ".\n";
   }

   delete def;
   return 0;
}
