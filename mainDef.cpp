#include <cstdio>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cfloat>

#include "deformable.h"
#include "gluvi.h"

using namespace std;

fptype timestep = 1.0 / 120;
fptype g[] = {-1, -3, 0};

Deformable* def;
fptype defMass;

// Gluvi stuff.
float target[] = {1.5, 1.0, 0};
Gluvi::Target3D cam(target, 3.0f, 0.0f, 0.0f, 60.0f, 0.01f, 100.0f);
double oldmousetime;
Vec2f oldmouse;
void display();
void mouse(int button, int state, int x, int y);
void drag(int x, int y);
void timer(int junk);

static bool drawFrames;
static float simTime = 0;
static float nextFrameTime = 0;
static int frameNumber;
static const float FRAME_TIME = timestep;

/**
 * Creates a simple deformable simulation.
 */
int main(int argc, char **argv)
{
   omp_set_num_threads(4);
   
   // Set up viewer stuff.
   Gluvi::init("Deformable", &argc, argv);
   Gluvi::camera=&cam;
   Gluvi::userDisplayFunc=display;
   Gluvi::userMouseFunc=mouse;
   Gluvi::userDragFunc=drag;
   glClearColor(1, 1, 1, 1);
   
   glutTimerFunc(1000, timer, 0);
   
   // Solid material parameters.
   fptype rho = 25;           // Density.
   fptype mu = 500;//10000;           // Second Lame parameter.
   fptype lambda = 500;//10000;     // First Lame parameter.
   fptype damping_m = 0;      // Mass damping coefficient.
   fptype damping_s = 0.025;//0.01;   // Stiffness damping coefficient.
   // Solid shape parameters.
   fptype sdx = 0.075;
   int nx = 2;//16;
   int ny = 14;//2;
   int nz = 2;//1;
   
   // Set up the solid.
   def = new CorotatedLinearDeformable();   
   def->createCube(sdx, nx / 2, ny / 2, nz / 2, false, false, true, false);
   //def->createFromFile("hollow_fixed.fm", 0.25);
   //def->createFromFile("dino.fm", 0.5);
   def->build(rho, mu, lambda, damping_m, damping_s);
   defMass = def->getVolume() * rho;
   
   // Transform the solid.
   //def->scale(1.5, 1, 1);
   //def->rotate(0, M_PI / 6, M_PI / 3);
   def->shift(0.75, 1, 0.75);
   def->setBounds(0, 10, 0, 10, -1, 1);
   
   drawFrames = argc > 1;

   Gluvi::run();

   return 0;
}


void display(void)
{
   glColor3f(1, 0, 0);
   glLineWidth(1);
   def->render();
}

void mouse(int button, int state, int x, int y)
{
   Vec2f newmouse;
   //cam.transform_mouse(x, y, newmouse.v);

   oldmouse=newmouse;
}

void drag(int x, int y)
{
   Vec2f newmouse;
   //cam.transform_mouse(x, y, newmouse.v);

   oldmouse=newmouse;
}

void timer(int junk)
{
   if(drawFrames && simTime >= nextFrameTime) {
      Gluvi::ppm_screenshot("img/frame%04d.ppm", frameNumber++);
      nextFrameTime += FRAME_TIME;
   }

   // For deformable simulation.
   def->move(timestep);
   def->addAcceleration(g, timestep);
   def->assembleStiffness();
   VectorXfp v = def->v;
   def->assembleFactoredStiffness();
   v -= def->v;
   SparseMatrixXfp diff = def->B - def->C.transpose() * def->C;
   std::cout << def->C.rows() << "\t\t" << def->C.cols() << "\t\t" << v.norm()
      << "\t\t" << diff.norm() << std::endl;
   def->step(timestep);
   
   simTime += timestep;
   if(simTime > 5) {
      exit(0);
   }

   glutPostRedisplay();
   glutTimerFunc(timestep, timer, 0);
}
