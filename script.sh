#!/bin/bash

FORMAT="user : %U\nsystem : %S\nelapsed : %E\nCPU : %P\nMemory:\n\ttext : %X\n\tdata : %D\n\tmax : %M\n\taverage : %K\ninputs : %I\noutputs : %O\nPagefaults:\n\tmajor : %F\n\tminor : %R\nswaps : %W"

/usr/bin/time -f "$FORMAT" -o timing.txt ./appC
./viewer 1
cd img
ffmpeg -framerate 30 -i screenshot%04d.ppm -c:v libx264 -r 30 -pix_fmt yuv420p out.mp4
cd ..
