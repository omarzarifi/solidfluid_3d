#include <cstdio>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cfloat>

#include "deformable.h"
#include "fluidsim.h"

using namespace std;

fptype timestep = 1.0 / 60;
fptype g[] = {0, -3, 0};

Deformable* def;
fptype defMass;
Fluid fluid;

void exportSurface(string path, int nn, int nb, int* boundary) {
   ofstream outfile(path.c_str());
   outfile << nn << " " << nb << endl;
   for(int i = 0; i < nb; i++) {
      outfile << boundary[3 * i] << " " << boundary[3 * i + 1] << " "
         << boundary[3 * i + 2] << endl;
   }
   outfile.close();
}

void exportFrame(string path, int frame, const VectorXfp& nodes,
   const vector<Vec3fp>& particles, fptype radius) {
   stringstream strout;
   strout << path << "frame_" << frame << ".txt";
   string filepath = strout.str();
   
   ofstream outfile(filepath.c_str());
   // Write vertex count and particle radius.
   outfile << particles.size() << " " << radius << endl;
   // Write vertices.
   for(int i = 0; i < particles.size(); i++) {
      outfile << particles[i][0] << " " << particles[i][1] << " "
         << particles[i][2] << endl;
   }
   
   // Write nodes.
   for(int i = 0; i < nodes.size() / 3; i++) {
      outfile << nodes[3 * i] << " " << nodes[3 * i + 1] << " "
         << nodes[3 * i + 2] << endl;
   }
   
   outfile.close();
}

int main(int argc, char **argv)
{
   omp_set_num_threads(4);
   
   // Solid material parameters.
   fptype rho = 1;           // Density.
   fptype mu = 750;            // Second Lame parameter.
   fptype lambda = mu;  // First Lame parameter.
   fptype damping_m = 0;      // Mass damping coefficient.
   fptype damping_s = 0.025;  // Stiffness damping coefficient.
   
   // Fluid parameters.
   int N = 82;                // Grid resolution.
   int M = 2;                 // Particles per grid cell per dimension.
   fptype dx = 1.0 / N;       // Grid cell size.
   fptype dx2 = dx / M;       // Particle separation.
   fptype rad = dx * 1.01 * sqrt(3.0) / 2.0;
      //2.5 / N / M;  // Particle radius.
   
   // Solid shift amounts.
   fptype sx = 0.46;//0.705;
   fptype sy = 0.21;//0.455;
   fptype sz = 0.46;//0.305;
   // Solid scale amounts.
   fptype scx = 1;
   fptype scy = 1;
   fptype scz = 1;
   // Solid shape parameters.
   int k = 1;
   fptype sdx = 0.04 / 2 / k;
   int nx = 4 * k;
   int ny = 28 * k;
   int nz = 4 * k;
   // Set up the solid.
   def = new CorotatedLinearDeformable();
   def->createCubeAsym(sdx, nx, ny, nz, false, false, false, true);
   //def->createCube(sdx, nx / 2, ny / 2, nz / 2, false, false, false, true);
   //def->createFromFile("dino.fm", 0.175);
   def->build(rho, mu, lambda, damping_m, damping_s);
   defMass = def->getVolume() * rho;
   def->scale(scx, scy, scz);
   //def->rotate(0, M_PI, 0);
   def->shift(sx, sy, sz);
   
   // Define the region not occupied by the fluid (due to the submerged solid).
   fptype sx0 = sx - rad / 2;
   fptype sx1 = sx + sdx * nx * scx + rad / 2;
   fptype sy0 = sy - rad / 2;
   fptype sy1 = sy + sdx * ny * scy + rad / 2;
   fptype sz0 = sz - rad / 2;
   fptype sz1 = sz + sdx * nz * scz + rad / 2;
   // Set up the fluid.
   fluid.initialize(25, N, rad);
   fluid.setProjectionMethod(PPM_FORCE_JACOBIAN);//_SPD);//_FAC2);
   fluid.setIterativeSolve(true);
   fluid.setUseCFL(false);
   fluid.setGravity(g[0], g[1], g[2]);
   fluid.writeToMatlab(true);
   fluid.setDeformable(def);
   fluid.setInfoStream("./timing_info.txt");
   for(int i = 1; i < N / 3; i++) {
      for(int j = 1; j < N - 1; j++) {
         for(int k = 1; k < N - 1; k++) {
            for(int k1 = 0; k1 < M; k1++) {
               for(int k2 = 0; k2 < M; k2++) {
                  for(int k3 = 0; k3 < M; k3++) {
                     Vec3fp pos(i * dx + (k1 + 0.5) * dx2,
                        j * dx + (k2 + 0.5) * dx2, k * dx + (k3 + 0.5) * dx2);
                     //if(!(pos[0] > sx0 && pos[0] < sx1 && pos[1] > sy0
                     //   && pos[1] < sy1 && pos[2] > sz0 && pos[2] < sz1)) {
                        fluid.addParticle(pos);
                     //}
                  }
               }
            }
         }
      }
   }
   /*for(int i = 1; i < N - 1; i++) {
      for(int j = 1; j < 6; j++) {
         for(int k = 1; k < N - 1; k++) {
            for(int k1 = 0; k1 < M; k1++) {
               for(int k2 = 0; k2 < M; k2++) {
                  for(int k3 = 0; k3 < M; k3++) {
                     Vec3fp pos(i * dx + (k1 + 0.5) * dx2,
                        j * dx + (k2 + 0.5) * dx2, k * dx + (k3 + 0.5) * dx2);
                     //if(!(pos[0] > sx0 && pos[0] < sx1 && pos[1] > sy0
                     //   && pos[1] < sy1 && pos[2] > sz0 && pos[2] < sz1)) {
                        fluid.addParticle(pos);
                     //}
                  }
               }
            }
         }
      }
   }
   
   fluid.addSphere(Vec3fp(0.5, 0.7, 0.85), 0.11);
   fluid.addSphere(Vec3fp(0.5, 0.5, 0.75), 0.09);
   
   // Give the fluid and solid some initial velocities.
   fptype accel[] = {0, 0, -1};
   //def->addAcceleration(accel, 0.5);
   fluid.addAccelerationAbove(accel, 3, 0.3);*/
   
   def->setBounds(dx, 1 - dx, dx, 1 - dx, dx, 1 - dx);
   //def->setBounds(0, 1, 0, 1, 0, 1);
   //fluid.solid_gravity = false;
   
   exportSurface("odata/surface.txt", def->n_v, def->n_b, def->boundary);
   exportFrame("odata/", 0, def->xs, fluid.particles, rad);
   for(int frame = 1; frame <= 100; frame++) {
      fluid.advance(timestep);
      exportFrame("odata/", frame, def->xs, fluid.particles, rad);
      cout << "Finished with frame " << frame << ".\n";
   }

   delete def;
   return 0;
}
