#include <cmath>
#include <fstream>
#include <limits>

#include "array3_utils.h"
#include "deformable.h"
#include "makelevelset3.h"
#include "util.h"

#include "Eigen/Eigenvalues"
#include "Eigen/SparseCholesky"

#ifdef __APPLE__
#include <GLUT/glut.h> // why does Apple have to put glut.h here...
#else
#include <GL/glut.h> // ...when everyone else puts it here?
#endif

// Number of exact bands to compute for the signed distance function (the
// remainder is approximated using marching cubes.
const static int EXACT_BANDS = 3;

/*****************************************************************************
 *                           HELPER FUNCTIONS                                *
 *****************************************************************************/

/**
 * This function returns the volume of a tetrahedron formed from the provided
 * vertices.
 */
fptype tetVolume(const Vector3fp &v0, const Vector3fp &v1, const Vector3fp &v2,
   const Vector3fp &v3) {
   Vector3fp temp1 = v1 - v0;
   Vector3fp temp2 = v2 - v0;
   return fabs(temp1.dot(temp2.cross(v3 - v0))) / 6;
}

/**
 * This function computes the local stiffness matrix for an element given the
 * material matrix and vertex locations (stacked in xs).
 */
Matrix12fp getStiffness(const Matrix9fp &LTKL, const Vector12fp &xs) {
   Matrix3fp Dm;
   Dm.col(0) = xs.segment<3>(3) - xs.segment<3>(0);
   Dm.col(1) = xs.segment<3>(6) - xs.segment<3>(0);
   Dm.col(2) = xs.segment<3>(9) - xs.segment<3>(0);
   Matrix3fp Dm_inv = Dm.inverse();
   
   Eigen::Matrix<fptype, 9, 12> M;
   M.setZero();
   for(int i = 0; i < 3; i++) {
      M(i, i) = -Dm_inv(0, 0) - Dm_inv(1, 0) - Dm_inv(2, 0);
      M(i + 3, i) = -Dm_inv(0, 1) - Dm_inv(1, 1) - Dm_inv(2, 1);
      M(i + 6, i) = -Dm_inv(0, 2) - Dm_inv(1, 2) - Dm_inv(2, 2);
      
      for(int j = 0; j < 3; j++) {
         for(int k = 0; k < 3; k++) {
            M(3 * i + k, 3 * j + k + 3) = Dm_inv(j, i);
         }
      }
   }
   
   return M.transpose() * LTKL * M;
}

/**
 * Gets the factor C for the local stiffness matrix, such that the stiffness
 * matrix is C'*C.
 */
Matrix6x12fp getFactoredStiffness(const Matrix6x9fp &kL, const Vector12fp &xs)
   {
   Matrix3fp Dm;
   Dm.col(0) = xs.segment<3>(3) - xs.segment<3>(0);
   Dm.col(1) = xs.segment<3>(6) - xs.segment<3>(0);
   Dm.col(2) = xs.segment<3>(9) - xs.segment<3>(0);
   Matrix3fp Dm_inv = Dm.inverse();
   
   Eigen::Matrix<fptype, 9, 12> M;
   M.setZero();
   for(int i = 0; i < 3; i++) {
      M(i, i) = -Dm_inv(0, 0) - Dm_inv(1, 0) - Dm_inv(2, 0);
      M(i + 3, i) = -Dm_inv(0, 1) - Dm_inv(1, 1) - Dm_inv(2, 1);
      M(i + 6, i) = -Dm_inv(0, 2) - Dm_inv(1, 2) - Dm_inv(2, 2);
      
      for(int j = 0; j < 3; j++) {
         for(int k = 0; k < 3; k++) {
            M(3 * i + k, 3 * j + k + 3) = Dm_inv(j, i);
         }
      }
   }
   
   return kL * M;
}

/**
 * This function clips a polygon to the voxel that stretches from lower to
 * upper. Input is a vector of vertices (without the last one duplicated); same
 * with output.
 */
void clipPolygon(const std::vector<Vec3fp>& input, std::vector<Vec3fp>& output,
   const Vec3fp& lower, const Vec3fp& upper) {
   output.clear();
   std::vector<Vec3fp> polygon = input;
   
   for(int dim = 0; dim < 3; ++dim) {
      for(size_t vert = 0; vert < polygon.size(); ++vert) {
         Vec3fp vert_pos = polygon[vert];
         Vec3fp next_pos = polygon[(vert + 1) % polygon.size()];
         
         bool cur_in = vert_pos[dim] >= lower[dim];
         bool next_in = next_pos[dim] >= lower[dim];
         
         if(cur_in && next_in) {
            output.push_back(next_pos);
         } else if(!cur_in && next_in) {
            fptype t = (lower[dim] - vert_pos[dim])
               / (next_pos[dim] - vert_pos[dim]);
            //assert(fabs(next_pos[dim] - vert_pos[dim]) > 1e-9);
            t = clamp(t, 0.0, 1.0);
            Vec3fp new_vert = lerp(vert_pos, next_pos, t);
            output.push_back(new_vert);
            output.push_back(next_pos);
         } else if(cur_in && !next_in) {
            fptype t = (lower[dim] - vert_pos[dim])
               / (next_pos[dim] - vert_pos[dim]);
            //assert(fabs(next_pos[dim] - vert_pos[dim]) > 1e-9);
            t = clamp(t, 0.0, 1.0);
            Vec3fp new_vert = lerp(vert_pos, next_pos, t);
            output.push_back(new_vert);
         }
      }
      
      polygon = output;
      output.clear();
   }

   for(int dim = 0; dim < 3; ++dim) {
      for(size_t vert = 0; vert < polygon.size(); ++vert) {
         Vec3fp vert_pos = polygon[vert];
         Vec3fp next_pos = polygon[(vert + 1) % polygon.size()];
         
         bool cur_in = vert_pos[dim] <= upper[dim];
         bool next_in = next_pos[dim] <= upper[dim];
         
         if(cur_in && next_in) {
            output.push_back(next_pos);
         } else if(!cur_in && next_in) {
            fptype t = (upper[dim] - vert_pos[dim])
               / (next_pos[dim] - vert_pos[dim]);
            //assert(fabs(next_pos[dim] - vert_pos[dim]) > 1e-9);
            t = clamp(t, 0.0, 1.0);
            Vec3fp new_vert = lerp(vert_pos, next_pos, t);
            output.push_back(new_vert);
            output.push_back(next_pos);
         } else if(cur_in && !next_in) {
            fptype t = (upper[dim] - vert_pos[dim])
               / (next_pos[dim] - vert_pos[dim]);
            //assert(fabs(next_pos[dim] - vert_pos[dim]) > 1e-9);
            t = clamp(t, 0.0, 1.0);
            Vec3fp new_vert = lerp(vert_pos, next_pos, t);
            output.push_back(new_vert);
         }
      }
      
      polygon = output;
      output.clear();
   }
   
   output = polygon;
}

/**
 * This function computes the area of a 3-dimensional planar polygon. It also
 * calculates the polygon's centroid.
 */
fptype area(const std::vector<Vec3fp>& p, const Vec3fp& normal,
   Vec3fp& centroid) {
   if(p.empty()) {
      return 0;
   }
   
   centroid = p[p.size() - 1];
   Vec3d cs = cross(p[p.size() - 1], p[0]);
   for(int i = 0; i < p.size() - 1; i++) {
      cs += cross(p[i], p[i + 1]);
      centroid += p[i];
   }
   
   centroid /= p.size();
   return fabs(dot(cs, normal)) / 2;
}

/**
 * Given barycentric coordinates with respect to axes of a tetrahedron, this
 * function measures how "close" the point is to being inside the tetrahedron.
 */
fptype barcError(const Vector3fp& barc) {
   fptype error = max(0.0, barc[0] - 1) + max(0.0, -barc[0]);
   error += max(0.0, barc[1] - 1) + max(0.0, -barc[1]);
   error += max(0.0, barc[2] - 1) + max(0.0, -barc[2]);
   fptype d = barc[0] + barc[1] + barc[2];
   error += max(0.0, -d) + max(0.0, 1 - d);
   return error;
}

/*****************************************************************************
 *                            BOUNDARY SAMPLE                                *
 *****************************************************************************/

BoundarySample::BoundarySample(fptype x_, fptype y_, fptype z_, fptype nx_,
   fptype ny_, fptype nz_, int n_index1_, int n_index2_, int n_index3_,
   fptype barc1_, fptype barc2_, fptype barc3_, int p_ind_) {
   n[0] = nx_;
   n[1] = ny_;
   n[2] = nz_;
   
   n_index[0] = n_index1_;
   n_index[1] = n_index2_;
   n_index[2] = n_index3_;
   
   barc[0] = barc1_;
   barc[1] = barc2_;
   barc[2] = barc3_;
   
   p_ind = p_ind_;
}

/*****************************************************************************
 *                               DEFORMABLE                                  *
 *****************************************************************************/

// NOTE: code for generating boundaries was tested on a 2x12x4 cuboid.
void Deformable::createCube(fptype dx, int nxh, int nyh, int nzh,
   bool constrainXL, bool constrainXR, bool constrainYL, bool constrainYR) {
   int nx = 2 * nxh;
   nx = nx == 0 ? 1 : nx;
   int ny = 2 * nyh;
   ny = ny == 0 ? 1 : ny;
   int nz = 2 * nzh;
   nz = nz == 0 ? 1 : nz;
   
   n_v = (nx + 1) * (ny + 1) * (nz + 1);
   n_t = 6 * nx * ny * nz;
   n_b = 4 * (nx * ny + nx * nz + ny * nz);
   
   xs.resize(3 * n_v);
   vs.resize(3 * n_v);
   vs.setZero();
   elements = new int[4 * n_t];
   boundary = new int[3 * n_b];
   constrained = new int[n_v];
   
   if(constrainXL || constrainXR || constrainYL || constrainYR) {
      int tempn = constrainXL && constrainXR ? 2 : 1;
      tempn = tempn == 1 && !constrainXL && !constrainXR ? 0 : tempn;
      int xSides = tempn;
      n_constrained = tempn * (ny + 1) * (nz + 1);
      
      tempn = constrainYL && constrainYR ? 2 : 1;
      tempn = tempn == 1 && !constrainYL && !constrainYR ? 0 : tempn;
      int ySides = tempn;
      n_constrained += tempn * (nx + 1) * (nz + 1);
      
      // Get rid of double-counted vertices.
      if(xSides > 0 && ySides > 0) {
         if(xSides == 2 && ySides == 2) {
            n_constrained -= 4 * (nz + 1);
         } else if(xSides == 2 || ySides == 2) {
            n_constrained -= 2 * (nz + 1);
         } else {
            n_constrained -= nz + 1;
         }
      }
      
      constrainedI = new int[n_constrained];
   } else {
      n_constrained = 0;
      constrainedI = NULL;
   }
   
   xs2.resize(3 * (n_v - n_constrained));
   vs2.resize(3 * (n_v - n_constrained));
   vs2.setZero();
   
   // Create the vertices.
   int curr = 0;
   int nb_curr = 0;
   int unconstrained = 0;
   for(int z = 0; z <= nz; z++) {
      fptype zc = z * dx;
      
      for(int y = 0; y <= ny; y++) {
         fptype yc = y * dx;
         
         for(int x = 0; x <= nx; x++) {
            bool constr = (constrainXL && x == 0) || (constrainXR && x == nx)
               || (constrainYL && y == 0) || (constrainYR && y == ny);
            if(constr) {
               constrainedI[nb_curr++] = curr / 3;
               constrained[curr / 3] = -1;
            } else {
               constrained[curr / 3] = unconstrained++;
            }
            
            xs[curr++] = x * dx;
            xs[curr++] = yc;
            xs[curr++] = zc;
         }
      }
   }
   
   // Create the tetrahedra.
   curr = 0;
   nb_curr = 0;
   for(int z = 0; z < nz; z++) {
      for(int y = 0; y < ny; y++) {
         for(int x = 0; x < nx; x++) {
            // Calculate the node indices for the current cube.
            int n1 = x + y * (nx + 1) + (z + 1) * (nx + 1) * (ny + 1);
            int n2 = x + y * (nx + 1) + z * (nx + 1) * (ny + 1);
            int n3 = n2 + 1;
            int n4 = n1 + 1;
            int n5 = x + (y + 1) * (nx + 1) + (z + 1) * (nx + 1) * (ny + 1);
            int n6 = x + (y + 1) * (nx + 1) + z * (nx + 1) * (ny + 1);
            int n7 = n6 + 1;
            int n8 = n5 + 1;
            
            // Swap some indices for symmetry.
            bool oddSwap = false;
            if(x >= nxh) {
               swap(n1, n4);
               swap(n2, n3);
               swap(n5, n8);
               swap(n6, n7);
               
               oddSwap = !oddSwap;
            }
            if(y >= nyh) {
               swap(n1, n5);
               swap(n2, n6);
               swap(n3, n7);
               swap(n4, n8);
               
               oddSwap = !oddSwap;
            }
            if(z >= nzh) {
               swap(n1, n2);
               swap(n3, n4);
               swap(n5, n6);
               swap(n7, n8);
               
               oddSwap = !oddSwap;
            }
            
            // Create the actual tetrahedra.
            // First element.
            elements[curr++] = n1;
            elements[curr++] = n2;
            elements[curr++] = n4;
            elements[curr++] = n8;
            // Second element.
            elements[curr++] = n1;
            elements[curr++] = n2;
            elements[curr++] = n5;
            elements[curr++] = n8;
            // Third element.
            elements[curr++] = n2;
            elements[curr++] = n3;
            elements[curr++] = n4;
            elements[curr++] = n6;
            // Fourth element.
            elements[curr++] = n2;
            elements[curr++] = n5;
            elements[curr++] = n6;
            elements[curr++] = n8;
            // Fifth element.
            elements[curr++] = n3;
            elements[curr++] = n4;
            elements[curr++] = n6;
            elements[curr++] = n7;
            // Sixth element.
            elements[curr++] = n4;
            elements[curr++] = n6;
            elements[curr++] = n7;
            elements[curr++] = n8;
            
            // Check if any tetrahedra are on the boundary.
            if(x == 0 || x == nx - 1) {
               if(oddSwap) {
                  boundary[nb_curr++] = n1;
                  boundary[nb_curr++] = n2;
                  boundary[nb_curr++] = n5;
                  
                  boundary[nb_curr++] = n2;
                  boundary[nb_curr++] = n6;
                  boundary[nb_curr++] = n5;
               } else {
                  boundary[nb_curr++] = n1;
                  boundary[nb_curr++] = n5;
                  boundary[nb_curr++] = n2;
                  
                  boundary[nb_curr++] = n2;
                  boundary[nb_curr++] = n5;
                  boundary[nb_curr++] = n6;
               }
            }
            if(y == 0 || y == ny - 1) {
               if(oddSwap) {
                  boundary[nb_curr++] = n1;
                  boundary[nb_curr++] = n4;
                  boundary[nb_curr++] = n2;
                  
                  boundary[nb_curr++] = n2;
                  boundary[nb_curr++] = n4;
                  boundary[nb_curr++] = n3;
               } else {
                  boundary[nb_curr++] = n1;
                  boundary[nb_curr++] = n2;
                  boundary[nb_curr++] = n4;
                  
                  boundary[nb_curr++] = n2;
                  boundary[nb_curr++] = n3;
                  boundary[nb_curr++] = n4;
               }
            }
            if(z == 0 || z == nz - 1) {
               if(oddSwap) {
                  boundary[nb_curr++] = n2;
                  boundary[nb_curr++] = n3;
                  boundary[nb_curr++] = n6;
                  
                  boundary[nb_curr++] = n3;
                  boundary[nb_curr++] = n7;
                  boundary[nb_curr++] = n6;
               } else {
                  boundary[nb_curr++] = n2;
                  boundary[nb_curr++] = n6;
                  boundary[nb_curr++] = n3;
                  
                  boundary[nb_curr++] = n3;
                  boundary[nb_curr++] = n6;
                  boundary[nb_curr++] = n7;
               }
            }
            
            // Add the extra boundary tetrahedra if there is only one layer.
            if(nxh == 0) {
               if(oddSwap) {
                  boundary[nb_curr++] = n3;
                  boundary[nb_curr++] = n4;
                  boundary[nb_curr++] = n7;
                  
                  boundary[nb_curr++] = n4;
                  boundary[nb_curr++] = n8;
                  boundary[nb_curr++] = n7;
               } else {
                  boundary[nb_curr++] = n3;
                  boundary[nb_curr++] = n7;
                  boundary[nb_curr++] = n4;
                  
                  boundary[nb_curr++] = n4;
                  boundary[nb_curr++] = n7;
                  boundary[nb_curr++] = n8;
               }
            }
            if(nyh == 0) {
               if(oddSwap) {
                  boundary[nb_curr++] = n5;
                  boundary[nb_curr++] = n6;
                  boundary[nb_curr++] = n8;
                  
                  boundary[nb_curr++] = n6;
                  boundary[nb_curr++] = n7;
                  boundary[nb_curr++] = n8;
               } else {
                  boundary[nb_curr++] = n5;
                  boundary[nb_curr++] = n8;
                  boundary[nb_curr++] = n6;
                  
                  boundary[nb_curr++] = n6;
                  boundary[nb_curr++] = n8;
                  boundary[nb_curr++] = n7;
               }
            }
            if(nzh == 0) {
               if(oddSwap) {
                  boundary[nb_curr++] = n1;
                  boundary[nb_curr++] = n8;
                  boundary[nb_curr++] = n4;
                  
                  boundary[nb_curr++] = n1;
                  boundary[nb_curr++] = n5;
                  boundary[nb_curr++] = n8;
               } else {
                  boundary[nb_curr++] = n1;
                  boundary[nb_curr++] = n4;
                  boundary[nb_curr++] = n8;
                  
                  boundary[nb_curr++] = n1;
                  boundary[nb_curr++] = n8;
                  boundary[nb_curr++] = n5;
               }
            }
         }
      }
   }
   
   // Create the bounding box.
   bb_minx = 0;
   bb_maxx = nx * dx;
   bb_miny = 0;
   bb_maxy = ny * dx;
   bb_minz = 0;
   bb_maxz = nz * dx;
}

void Deformable::createCubeAsym(fptype dx, int nx, int ny, int nz,
   bool constrainXL, bool constrainXR, bool constrainYL, bool constrainYR) {
   
   n_v = (nx + 1) * (ny + 1) * (nz + 1);
   n_t = 6 * nx * ny * nz;
   n_b = 4 * (nx * ny + nx * nz + ny * nz);
   
   xs.resize(3 * n_v);
   vs.resize(3 * n_v);
   vs.setZero();
   elements = new int[4 * n_t];
   boundary = new int[3 * n_b];
   constrained = new int[n_v];
   
   if(constrainXL || constrainXR || constrainYL || constrainYR) {
      int tempn = constrainXL && constrainXR ? 2 : 1;
      tempn = tempn == 1 && !constrainXL && !constrainXR ? 0 : tempn;
      int xSides = tempn;
      n_constrained = tempn * (ny + 1) * (nz + 1);
      
      tempn = constrainYL && constrainYR ? 2 : 1;
      tempn = tempn == 1 && !constrainYL && !constrainYR ? 0 : tempn;
      int ySides = tempn;
      n_constrained += tempn * (nx + 1) * (nz + 1);
      
      // Get rid of double-counted vertices.
      if(xSides > 0 && ySides > 0) {
         if(xSides == 2 && ySides == 2) {
            n_constrained -= 4 * (nz + 1);
         } else if(xSides == 2 || ySides == 2) {
            n_constrained -= 2 * (nz + 1);
         } else {
            n_constrained -= nz + 1;
         }
      }
      
      constrainedI = new int[n_constrained];
   } else {
      n_constrained = 0;
      constrainedI = NULL;
   }
   
   xs2.resize(3 * (n_v - n_constrained));
   vs2.resize(3 * (n_v - n_constrained));
   vs2.setZero();
   
   // Create the vertices.
   int curr = 0;
   int nb_curr = 0;
   int unconstrained = 0;
   for(int z = 0; z <= nz; z++) {
      fptype zc = z * dx;
      
      for(int y = 0; y <= ny; y++) {
         fptype yc = y * dx;
         
         for(int x = 0; x <= nx; x++) {
            bool constr = (constrainXL && x == 0) || (constrainXR && x == nx)
               || (constrainYL && y == 0) || (constrainYR && y == ny);
            if(constr) {
               constrainedI[nb_curr++] = curr / 3;
               constrained[curr / 3] = -1;
            } else {
               constrained[curr / 3] = unconstrained++;
            }
            
            xs[curr++] = x * dx;
            xs[curr++] = yc;
            xs[curr++] = zc;
         }
      }
   }
   
   // Create the tetrahedra.
   curr = 0;
   nb_curr = 0;
   for(int z = 0; z < nz; z++) {
      for(int y = 0; y < ny; y++) {
         for(int x = 0; x < nx; x++) {
            // Calculate the node indices for the current cube.
            int n1 = x + y * (nx + 1) + (z + 1) * (nx + 1) * (ny + 1);
            int n2 = x + y * (nx + 1) + z * (nx + 1) * (ny + 1);
            int n3 = n2 + 1;
            int n4 = n1 + 1;
            int n5 = x + (y + 1) * (nx + 1) + (z + 1) * (nx + 1) * (ny + 1);
            int n6 = x + (y + 1) * (nx + 1) + z * (nx + 1) * (ny + 1);
            int n7 = n6 + 1;
            int n8 = n5 + 1;
            
            // Create the actual tetrahedra.
            // First element.
            elements[curr++] = n1;
            elements[curr++] = n2;
            elements[curr++] = n4;
            elements[curr++] = n8;
            // Second element.
            elements[curr++] = n1;
            elements[curr++] = n2;
            elements[curr++] = n5;
            elements[curr++] = n8;
            // Third element.
            elements[curr++] = n2;
            elements[curr++] = n3;
            elements[curr++] = n4;
            elements[curr++] = n6;
            // Fourth element.
            elements[curr++] = n2;
            elements[curr++] = n5;
            elements[curr++] = n6;
            elements[curr++] = n8;
            // Fifth element.
            elements[curr++] = n3;
            elements[curr++] = n4;
            elements[curr++] = n6;
            elements[curr++] = n7;
            // Sixth element.
            elements[curr++] = n4;
            elements[curr++] = n6;
            elements[curr++] = n7;
            elements[curr++] = n8;
            
            // Check if any tetrahedra are on the boundary.
            if(x == 0 || x == nx - 1) {
               boundary[nb_curr++] = n1;
               boundary[nb_curr++] = n5;
               boundary[nb_curr++] = n2;
               
               boundary[nb_curr++] = n2;
               boundary[nb_curr++] = n5;
               boundary[nb_curr++] = n6;
            }
            if(y == 0 || y == ny - 1) {
               boundary[nb_curr++] = n1;
               boundary[nb_curr++] = n2;
               boundary[nb_curr++] = n4;
               
               boundary[nb_curr++] = n2;
               boundary[nb_curr++] = n3;
               boundary[nb_curr++] = n4;
            }
            if(z == 0 || z == nz - 1) {
               boundary[nb_curr++] = n2;
               boundary[nb_curr++] = n6;
               boundary[nb_curr++] = n3;
               
               boundary[nb_curr++] = n3;
               boundary[nb_curr++] = n6;
               boundary[nb_curr++] = n7;
            }
            
            // Add the extra boundary tetrahedra if there is only one layer.
            if(nx == 1) {
               boundary[nb_curr++] = n3;
               boundary[nb_curr++] = n7;
               boundary[nb_curr++] = n4;
               
               boundary[nb_curr++] = n4;
               boundary[nb_curr++] = n7;
               boundary[nb_curr++] = n8;
            }
            if(ny == 1) {
               boundary[nb_curr++] = n5;
               boundary[nb_curr++] = n8;
               boundary[nb_curr++] = n6;
               
               boundary[nb_curr++] = n6;
               boundary[nb_curr++] = n8;
               boundary[nb_curr++] = n7;
            }
            if(nz == 1) {
               boundary[nb_curr++] = n1;
               boundary[nb_curr++] = n4;
               boundary[nb_curr++] = n8;
               
               boundary[nb_curr++] = n1;
               boundary[nb_curr++] = n8;
               boundary[nb_curr++] = n5;
            }
         }
      }
   }
   
   // Create the bounding box.
   bb_minx = 0;
   bb_maxx = nx * dx;
   bb_miny = 0;
   bb_maxy = ny * dx;
   bb_minz = 0;
   bb_maxz = nz * dx;
}

void Deformable::createFromFile(const char* file, fptype scale) {
   std::ifstream meshFile(file);
   if(!meshFile.good()) {
      std::cout << "Failed to open mesh file.\n";
      exit(0);
   }
   
   n_constrained = 0;
   constrainedI = NULL;
   
   bb_minx = std::numeric_limits<fptype>::max();
   bb_maxx = -std::numeric_limits<fptype>::max();
   bb_miny = std::numeric_limits<fptype>::max();
   bb_maxy = -std::numeric_limits<fptype>::max();
   bb_minz = std::numeric_limits<fptype>::max();
   bb_maxz = -std::numeric_limits<fptype>::max();
   
   // Read node information.
   meshFile >> n_v;
   xs.resize(3 * n_v);
   vs.resize(3 * n_v);
   vs.setZero();
   constrained = new int[n_v];
   for(int i = 0; i < n_v; i++) {
      meshFile >> xs[3 * i] >> xs[3 * i + 1] >> xs[3 * i + 2];
      
      // Apply the scaling to the mesh.
      xs[3 * i] *= scale;
      xs[3 * i + 1] *= scale;
      xs[3 * i + 2] *= scale;
      
      if(xs[3 * i] < bb_minx) {
         bb_minx = xs[3 * i];
      } else if(xs[3 * i] > bb_maxx) {
         bb_maxx = xs[3 * i];
      }
      
      if(xs[3 * i + 1] < bb_miny) {
         bb_miny = xs[3 * i + 1];
      } else if(xs[3 * i + 1] > bb_maxy) {
         bb_maxy = xs[3 * i + 1];
      }
      
      if(xs[3 * i + 2] < bb_minz) {
         bb_minz = xs[3 * i + 2];
      } else if(xs[3 * i + 2] > bb_maxz) {
         bb_maxz = xs[3 * i + 2];
      }
      
      constrained[i] = i;
   }
   // Copy to the computational vectors.
   xs2 = xs;
   vs2 = vs;
   
   // Read the boundary information.
   meshFile >> n_b;
   boundary = new int[3 * n_b];
   for(int i = 0; i < n_b; i++) {
      meshFile >> boundary[3 * i] >> boundary[3 * i + 1]
         >> boundary[3 * i + 2];
   }
   
   // Read the tetrahedra information.
   meshFile >> n_t;
   elements = new int[4 * n_t];
   for(int i = 0; i < n_t; i++) {
      meshFile >> elements[4 * i] >> elements[4 * i + 1] >> elements[4 * i + 2]
         >> elements[4 * i + 3];
   }
   
   // Finish up.
   meshFile.close();
}

void Deformable::shift(fptype x, fptype y, fptype z) {
   for(int i = 0; i < n_v; i++) {
      xs[3 * i] += x;
      xs[3 * i + 1] += y;
      xs[3 * i + 2] += z;
   }
   
   bb_minx += x;
   bb_maxx += x;
   bb_miny += y;
   bb_maxy += y;
   bb_minz += z;
   bb_maxz += z;
}

void Deformable::rotate(fptype angleX, fptype angleY, fptype angleZ) {
   bb_minx = std::numeric_limits<fptype>::max();
   bb_maxx = -std::numeric_limits<fptype>::max();
   bb_miny = std::numeric_limits<fptype>::max();
   bb_maxy = -std::numeric_limits<fptype>::max();
   bb_minz = std::numeric_limits<fptype>::max();
   bb_maxz = -std::numeric_limits<fptype>::max();
   
   // Form the rotation matrix.
   Eigen::Matrix<fptype, 3, 3> rotX;
   Eigen::Matrix<fptype, 3, 3> rotY;
   Eigen::Matrix<fptype, 3, 3> rotZ;
   rotX << 1, 0, 0, 0, cos(angleX), -sin(angleX), 0, sin(angleX), cos(angleX);
   rotY << cos(angleY), 0, sin(angleY), 0, 1, 0, -sin(angleY), 0, cos(angleY);
   rotZ << cos(angleZ), -sin(angleZ), 0, sin(angleZ), cos(angleZ), 0, 0, 0, 1;
   Eigen::Matrix<fptype, 3, 3> rot = rotZ * rotY * rotX;
   
   for(int i = 0; i < n_v; i++) {
      xs.segment<3>(3 * i) = rot * xs.segment<3>(3 * i);
      
      bb_minx = min(xs[3 * i], bb_minx);
      bb_maxx = max(xs[3 * i], bb_maxx);
      bb_miny = min(xs[3 * i + 1], bb_miny);
      bb_maxy = max(xs[3 * i + 1], bb_maxy);
      bb_miny = min(xs[3 * i + 2], bb_minz);
      bb_maxy = max(xs[3 * i + 2], bb_maxz);
   }
}

void Deformable::scale(fptype sx, fptype sy, fptype sz) {
   for(int i = 0; i < n_v; i++) {
      xs[3 * i] *= sx;
      xs[3 * i + 1] *= sy;
      xs[3 * i + 2] *= sz;
   }
   
   bb_minx *= sx;
   bb_maxx *= sx;
   bb_miny *= sy;
   bb_maxy *= sy;
   bb_minz *= sz;
   bb_maxz *= sz;
}

void Deformable::move(fptype dt) {
   bb_minx = xbounds[1];
   bb_maxx = xbounds[0];
   bb_miny = ybounds[1];
   bb_maxy = ybounds[0];
   bb_minz = zbounds[1];
   bb_maxz = zbounds[0];
   
   for(int i = 0; i < n_v; i++) {
      for(int j = 0; j < 3; j++) {
         fptype* bounds;
         fptype* bb_min;
         fptype* bb_max;
         if(j == 0) {
            bounds = xbounds;
            bb_min = &bb_minx;
            bb_max = &bb_maxx;
         } else if(j == 1) {
            bounds = ybounds;
            bb_min = &bb_miny;
            bb_max = &bb_maxy;
         } else {
            bounds = zbounds;
            bb_min = &bb_miny;
            bb_max = &bb_maxy;
         }
         
         xs[3 * i + j] += dt * vs[3 * i + j];
         if(xs[3 * i + j] < bounds[0] && vs[3 * i + j] < 0) {
            xs[3 * i + j] = bounds[0];
            vs[3 * i + j] = 0;
            vs2[3 * constrained[i] + j] = 0;
         } else if(xs[3 * i + j] > bounds[1] && vs[3 * i + j] > 0) {
            xs[3 * i + j] = bounds[1];
            vs[3 * i + j] = 0;
            vs2[3 * constrained[i] + j] = 0;
         }
         
         // Copy to the reduced position vectors.
         if(constrained[i] >= 0) {
            xs2[3 * constrained[i] + j] = xs[3 * i + j];
         }
         
         *bb_min = min(*bb_min, xs[3 * i + j]);
         *bb_max = max(*bb_max, xs[3 * i + j]);
      }
   }
}

void Deformable::step(fptype dt) {
   SparseMatrixXfp Mdt = M / dt;
   
   // Compute the right hand side of the system.
   VectorXfp rhs = Mdt * vs2 - B * xs2 + v;
   
   Eigen::ConjugateGradient<SparseMatrixXfp, Eigen::Lower | Eigen::Upper,
      Eigen::IncompleteCholesky<fptype> > solver;
   solver.setTolerance(1e-10);
   solver.compute(Mdt + dt * B - D);
   if(solver.info() != Eigen::Success) {
      std::cout << "Eigen factorization failed.\n";
      exit(0);
   }
   
   vs2 = solver.solveWithGuess(rhs, vs2);
   if(solver.info() != Eigen::Success) {
      std::cout << "Eigen solve failed.\n";
      std::cout << "Error: " << solver.error() << std::endl;
      exit(0);
   }
   
   // Copy the velocities to the full vector.
   for(int i = 0; i < n_v; i++) {
      if(constrained[i] >= 0) {
         vs[3 * i] = vs2[3 * constrained[i]];
         vs[3 * i + 1] = vs2[3 * constrained[i] + 1];
         vs[3 * i + 2] = vs2[3 * constrained[i] + 2];
      }
   }
}

void Deformable::stepE(fptype dt) {
   SparseMatrixXfp Mdt = M / dt;
   
   // Compute the right hand side of the system.
   VectorXfp rhs = Mdt * vs2 - B * xs2 + v;
   
   Eigen::ConjugateGradient<SparseMatrixXfp, Eigen::Lower | Eigen::Upper,
      Eigen::IncompleteCholesky<fptype> > solver;
   solver.setTolerance(1e-10);
   solver.compute(Mdt + dt * B);
   if(solver.info() != Eigen::Success) {
      std::cout << "Eigen factorization failed.\n";
      exit(0);
   }
   
   vs2 = solver.solveWithGuess(rhs, vs2);
   if(solver.info() != Eigen::Success) {
      std::cout << "Eigen solve failed.\n";
      std::cout << "Error: " << solver.error() << std::endl;
      exit(0);
   }
   
   // Copy the velocities to the full vector.
   for(int i = 0; i < n_v; i++) {
      if(constrained[i] >= 0) {
         vs[3 * i] = vs2[3 * constrained[i]];
         vs[3 * i + 1] = vs2[3 * constrained[i] + 1];
         vs[3 * i + 2] = vs2[3 * constrained[i] + 2];
      }
   }
}

void Deformable::addAcceleration(fptype a[3], fptype dt) {
   fptype adt[] = {a[0] * dt, a[1] * dt, a[2] * dt};
   
   for(int i = 0; i < 3 * (n_v - n_constrained); i++) {
      vs2[i] += adt[i % 3];
   }
}

void Deformable::computeSolidPhi(fptype dx, Array3fp& nodal_phi) {
   make_level_set3(boundary, xs, dx, n_b, nodal_phi, EXACT_BANDS);
}

void Deformable::intersectGrid(std::vector<BoundarySample>& b, fptype dx,
   const Array3i& F, fptype minArea) {
   b.clear();
   
   std::vector<Vec3fp> clipped;
   std::vector<Vec3fp> triangle;
   triangle.push_back(Vec3fp(0));
   triangle.push_back(Vec3fp(0));
   triangle.push_back(Vec3fp(0));
   
   for(int i = 0; i < n_b; i++) {
      // First grab the vertices of the current boundary triangle.
      triangle[0] = Vec3fp(xs[3 * boundary[3 * i]],
         xs[3 * boundary[3 * i] + 1], xs[3 * boundary[3 * i] + 2]);
      triangle[1] = Vec3fp(xs[3 * boundary[3 * i + 1]],
         xs[3 * boundary[3 * i + 1] + 1], xs[3 * boundary[3 * i + 1] + 2]);
      triangle[2] = Vec3fp(xs[3 * boundary[3 * i + 2]],
         xs[3 * boundary[3 * i + 2] + 1], xs[3 * boundary[3 * i + 2] + 2]);
      
      // Compute the triangle normal.
      Vec3fp normal = cross(triangle[1] - triangle[0],
         triangle[2] - triangle[0]);
      normalize(normal);
      
      // Get the axis-aligned bounding box for the triangle.
      fptype mins[] = {min(triangle[0][0], triangle[1][0], triangle[2][0]),
         min(triangle[0][1], triangle[1][1], triangle[2][1]),
         min(triangle[0][2], triangle[1][2], triangle[2][2])};
      fptype maxs[] = {max(triangle[0][0], triangle[1][0], triangle[2][0]),
         max(triangle[0][1], triangle[1][1], triangle[2][1]),
         max(triangle[0][2], triangle[1][2], triangle[2][2])};
      
      // Clip the triangle against all possible cells.
      Vec3fp lower, upper, centroid;
      for(int z = std::floor(mins[2] / dx); z < std::ceil(maxs[2] / dx); z++) {
         lower[2] = z * dx;
         upper[2] = (z + 1) * dx;
         
         for(int y = std::floor(mins[1] / dx); y < std::ceil(maxs[1] / dx);
            y++) {
            lower[1] = y * dx;
            upper[1] = (y + 1) * dx;
            
            for(int x = std::floor(mins[0] / dx); x < std::ceil(maxs[0] / dx);
               x++) {
               if(F(x, y, z) >= 0) {
                  lower[0] = x * dx;
                  upper[0] = (x + 1) * dx;
                  
                  // Perform the actual clipping.
                  clipPolygon(triangle, clipped, lower, upper);
                  fptype a = area(clipped, normal, centroid);
                  
                  // Create the boundary segment if there was an intersection.
                  if(a > minArea) {
                     // Find the barycentric coordinates of the centroid.
                     Vector3fp c(centroid[0] - triangle[0][0],
                        centroid[1] - triangle[0][1],
                        centroid[2] - triangle[0][2]);
                     Matrix3x2fp edges;
                     edges << triangle[1][0] - triangle[0][0],
                        triangle[2][0] - triangle[0][0],
                        triangle[1][1] - triangle[0][1],
                        triangle[2][1] - triangle[0][1],
                        triangle[1][2] - triangle[0][2],
                        triangle[2][2] - triangle[0][2];
                     Vector2fp bc = edges.colPivHouseholderQr().solve(c);
                     
                     // Finally, add the segment to the list.
                     b.push_back(BoundarySample(centroid[0], centroid[1],
                        centroid[2], -a * normal[0], -a * normal[1],
                        -a * normal[2], constrained[boundary[3 * i]],
                        constrained[boundary[3 * i + 1]],
                        constrained[boundary[3 * i + 2]], 1 - bc[0] - bc[1],
                        bc[0], bc[1], F(x, y, z)));
                  }
               }
            }
         }
      }
   }
}

void Deformable::projectNormalV(fptype dx, Array3fp& v, const Array3fp& valid,
   const Array3fp& phi, const Fluid& f, int dir) {
   #pragma omp parallel for
   for(int i = 0; i < n_t; i++) {
      // Get the node locations.
      Vector3fp v0 = xs.segment<3>(3 * elements[4 * i]);
      Vector3fp v1 = xs.segment<3>(3 * elements[4 * i + 1]);
      Vector3fp v2 = xs.segment<3>(3 * elements[4 * i + 2]);
      Vector3fp v3 = xs.segment<3>(3 * elements[4 * i + 3]);
      // Get the node velocities.
      Vector3fp vel0 = vs.segment<3>(3 * elements[4 * i]);
      Vector3fp vel1 = vs.segment<3>(3 * elements[4 * i + 1]);
      Vector3fp vel2 = vs.segment<3>(3 * elements[4 * i + 2]);
      Vector3fp vel3 = vs.segment<3>(3 * elements[4 * i + 3]);
      
      // Factor the edge matrix for this tet.
      Matrix3fp tet;
      tet.col(0) = v1 - v0;
      tet.col(1) = v2 - v0;
      tet.col(2) = v3 - v0;
      Eigen::PartialPivLU<Matrix3fp> lu = tet.lu();
      
      // Find the range of indices for which the sample could fall inside the 
      // tetrahedron.
      int range0[3];
      int range1[3];
      for(int j = 0; j < 3; j++) {
         fptype mi = min(v0[j], v1[j], v2[j], v3[j]);
         fptype ma = max(v0[j], v1[j], v2[j], v3[j]);
         
         if(dir == j) {
            range0[j] = std::floor(mi / dx);
            range1[j] = std::ceil(ma / dx);
         } else {
            range0[j] = std::floor(mi / dx - 0.5);
            range1[j] = std::ceil(ma / dx - 0.5);
         }
      }
      
      // Loop over the relevant regions of the array.
      for(int z = range0[2]; z < range1[2]; z++) {
         for(int y = range0[1]; y < range1[1]; y++) {
            for(int x = range0[0]; x < range1[0]; x++) {
               if(valid(x, y, z) > 0) {
                  Vec3fp loc(x + 0.5, y + 0.5, z + 0.5);
                  loc[dir] -= 0.5;
                  Vec3fp locDX0 = loc * dx;
                  Vector3fp locDX(locDX0[0], locDX0[1], locDX0[2]);
                  
                  // Find the barycentric coordinates of the point.
                  Vector3fp barc = lu.solve(locDX - v0);
                  if(barc[0] >= 0 && barc[1] >= 0 && barc[2] >= 0 &&
                     barc[0] + barc[1] + barc[2] <= 1) {
                     // Compute the velocities.
                     Vec3fp vel_f0 = f.getVelocity(locDX0);
                     Vector3fp vel_f(vel_f0[0], vel_f0[1], vel_f0[2]);
                     Vector3fp vel_s = barc[0] * vel0 + barc[1] * vel1
                        + barc[2] * vel2 + (1 - barc[0] - barc[1] - barc[2])
                        * vel3;
                     
                     // Get the normal vector.
                     Vec3fp normal;
                     interpolate_gradient(normal, loc, phi);
                     normalize(normal);
                     Vector3fp n(normal[0], normal[1], normal[2]);
            
                     // Project out the normal component.
                     vel_f += n.dot(vel_s - vel_f) * n;
                     v(x, y, z) = vel_f[dir];
                  }
               }
            }
         }
      }
   }
}

Vec3fp Deformable::getVelocity(const Vec3fp& location) {
   Matrix3fp tet;
   Vector3fp loc(location[0], location[1], location[2]);
   fptype minError = 1e100;
   int minTet;
   Vector3fp minCoord;
   
   for(int i = 0; i < n_t; i++) {
      tet.col(0) = xs.segment<3>(3 * elements[4 * i + 1])
         - xs.segment<3>(3 * elements[4 * i]);
      tet.col(1) = xs.segment<3>(3 * elements[4 * i + 2])
         - xs.segment<3>(3 * elements[4 * i]);
      tet.col(2) = xs.segment<3>(3 * elements[4 * i + 3])
         - xs.segment<3>(3 * elements[4 * i]);
      Vector3fp coordinates = tet.lu().solve(loc
         - xs.segment<3>(3 * elements[4 * i]));
      fptype error = barcError(coordinates);
      
      if(error == 0) {
         Vector3fp r = coordinates[0] * vs.segment<3>(3 * elements[4 * i + 1])
            + coordinates[1] * vs.segment<3>(3 * elements[4 * i + 2])
            + coordinates[2] * vs.segment<3>(3 * elements[4 * i + 3])
            + (1 - coordinates[0] - coordinates[1] - coordinates[2])
            * vs.segment<3>(3 * elements[4 * i]);
         return Vec3fp(r[0], r[1], r[2]);
      } else if(error < minError) {
         minError = error;
         minTet = i;
         minCoord = coordinates;
      }
   }
   
   /*Vector3fp r = minCoord[0] * vs.segment<3>(3 * elements[4 * minTet + 1])
      + minCoord[1] * vs.segment<3>(3 * elements[4 * minTet + 2])
      + minCoord[2] * vs.segment<3>(3 * elements[4 * minTet + 3])
      + (1 - minCoord[0] - minCoord[1] - minCoord[2])
      * vs.segment<3>(3 * elements[4 * minTet]);
   return Vec3fp(r[0], r[1], r[2]);*/
   return Vec3fp(0, 0, 0);
}

void Deformable::setBounds(fptype x0, fptype x1, fptype y0, fptype y1,
   fptype z0, fptype z1) {
   xbounds[0] = x0;
   xbounds[1] = x1;
   ybounds[0] = y0;
   ybounds[1] = y1;
   zbounds[0] = z0;
   zbounds[1] = z1;
}

fptype Deformable::maxV() {
   fptype maxV = 0;
   
   for(int i = 0; i < 3 * n_v; i++) {
      maxV = max(maxV, (fptype) fabs(vs[i]));
   }
   
   return maxV;
}

fptype Deformable::getVolume() {
   fptype volume = 0;
   
   for(int i = 0; i < n_t; i++) {
      volume += tetVolume(xs.segment<3>(3 * elements[4 * i]),
         xs.segment<3>(3 * elements[4 * i + 1]),
         xs.segment<3>(3 * elements[4 * i + 2]),
         xs.segment<3>(3 * elements[4 * i + 3]));
   }
   
   return volume;
}

void Deformable::render() {
   float vert[3];
   
   glBegin(GL_LINES);
   for(int i = 0; i < n_b; i++) {
      for(int j = 0; j < 3; j++) {
         int temp = boundary[3 * i + j];
         vert[0] = xs[3 * temp];
         vert[1] = xs[3 * temp + 1];
         vert[2] = xs[3 * temp + 2];
         glVertex3fv(vert);
         
         temp = boundary[3 * i + (j + 1) % 3];
         vert[0] = xs[3 * temp];
         vert[1] = xs[3 * temp + 1];
         vert[2] = xs[3 * temp + 2];
         glVertex3fv(vert);
      }
   }
   glEnd();
}

/*****************************************************************************
 *                        COROTATEDLINEARDEFORMABLE                          *
 *****************************************************************************/

CorotatedLinearDeformable::CorotatedLinearDeformable() {
   n_v = 0;
   
   xbounds[0] = -std::numeric_limits<fptype>::max();
   xbounds[1] = std::numeric_limits<fptype>::max();
   ybounds[0] = -std::numeric_limits<fptype>::max();
   ybounds[1] = std::numeric_limits<fptype>::max();
   zbounds[0] = -std::numeric_limits<fptype>::max();
   zbounds[1] = std::numeric_limits<fptype>::max();
}

void CorotatedLinearDeformable::build(fptype rho, fptype mu, fptype lambda,
   fptype dm, fptype ds) {   
   // Material matrix used to compute the stiffness matrices.
   fptype l_2m = lambda + 2 * mu;
   Matrix9fp LTKL;
   LTKL << l_2m, 0, 0, 0, lambda, 0, 0, 0, lambda,
      0, mu, 0, mu, 0, 0, 0, 0, 0,
      0, 0, mu, 0, 0, 0, mu, 0, 0,
      0, mu, 0, mu, 0, 0, 0, 0, 0,
      lambda, 0, 0, 0, l_2m, 0, 0, 0, lambda,
      0, 0, 0, 0, 0, mu, 0, mu, 0,
      0, 0, mu, 0, 0, 0, mu, 0, 0,
      0, 0, 0, 0, 0, mu, 0, mu, 0,
      lambda, 0, 0, 0, lambda, 0, 0, 0, l_2m;
   // Factored material matrix.
   fptype sqr_m = sqrt(mu);
   fptype sqr_l_m = sqrt(lambda + mu);
   fptype sqr_l_2m = sqrt(l_2m);
   Matrix6x9fp kL;
   kL << sqr_l_2m, 0, 0, 0, lambda / sqr_l_2m, 0, 0, 0, lambda / sqr_l_2m,
      0, sqr_m, 0, sqr_m, 0, 0, 0, 0, 0,
      0, 0, sqr_m, 0, 0, 0, sqr_m, 0, 0,
      0, 0, 0, 0, 2 * sqr_m * sqr_l_m / sqr_l_2m, 0, 0, 0,
         lambda * sqr_m / (sqr_l_m * sqr_l_2m),
      0, 0, 0, 0, 0, sqr_m, 0, sqr_m, 0,
      0, 0, 0, 0, 0, 0, 0, 0, sqr_m * sqrt(2 * lambda + l_2m) / sqr_l_m;
   
   // Areas assigned to all tetrahedra.
   int dim = 3 * (n_v - n_constrained);
   VectorXfp masses(dim);
   masses.setZero();
   
   // Loop over the tetrahedra.
   for(int i = 0; i < n_t; i++) {
      int curr_ti = 4 * i;
      
      // Computes the local stiffness matrix for this element.
      // Set the edge matrix.
      int node0 = elements[curr_ti];
      int node1 = elements[curr_ti + 1];
      int node2 = elements[curr_ti + 2];
      int node3 = elements[curr_ti + 3];
      Vector12fp currXs;
      currXs.segment<3>(0) = xs.segment<3>(3 * node0);
      currXs.segment<3>(3) = xs.segment<3>(3 * node1);
      currXs.segment<3>(6) = xs.segment<3>(3 * node2);
      currXs.segment<3>(9) = xs.segment<3>(3 * node3);
      fptype volume = tetVolume(currXs.segment<3>(0), currXs.segment<3>(3),
         currXs.segment<3>(6), currXs.segment<3>(9));
      Matrix12fp currB = volume * getStiffness(LTKL, currXs);
      localBs.push_back(currB);
      localvs.push_back(currB * currXs);
      localBsFactored.push_back(sqrt(volume) * getFactoredStiffness(kL,
         currXs));
      
      // Compute the inverse of the barycentric coordinate matrix.
      Matrix4fp P;
      P.col(0) << currXs[0], currXs[1], currXs[2], 1;
      P.col(1) << currXs[3], currXs[4], currXs[5], 1;
      P.col(2) << currXs[6], currXs[7], currXs[8], 1;
      P.col(3) << currXs[9], currXs[10], currXs[11], 1;
      Pinvs.push_back(P.inverse());
      
      // Distribute the mass among the nodes.
      for(int j = 0; j < 3; j++) {
         if(constrained[node0] >= 0) {
            masses[3 * constrained[node0] + j] += rho * volume / 4;
         }
         if(constrained[node1] >= 0) {
            masses[3 * constrained[node1] + j] += rho * volume / 4;
         }
         if(constrained[node2] >= 0) {
            masses[3 * constrained[node2] + j] += rho * volume / 4;
         }
         if(constrained[node3] >= 0) {
            masses[3 * constrained[node3] + j] += rho * volume / 4;
         }
      }
   }
   
   // Appropriately resize and initialize the matrices.
   M.resize(dim, dim);
   Minv.resize(dim, dim);
   B.resize(dim, dim);
   C.resize(6 * n_t, dim);
   v.resize(dim);
   
   // Set the lumped mass matrix.
   M = masses.asDiagonal();
   for(int i = 0; i < dim; i++) {
      masses[i] = 1 / masses[i];
   }
   Minv = masses.asDiagonal();
   
   // Set Rayleigh damping coefficients.
   d_m = dm;
   d_s = ds;
}

void CorotatedLinearDeformable::assembleStiffness() {
   v.setZero();
   
   // Triplets used for sparse matrix initialization.
   std::vector<Eigen::Triplet<fptype> > Bts;
   
   Matrix12fp R;
   R.setZero();
   for(int i = 0; i < n_t; i++) {
      // Grab the node indices.
      int curr_ti = 4 * i;
      int inds[] = {elements[curr_ti], elements[curr_ti + 1],
         elements[curr_ti + 2], elements[curr_ti + 3]};
      
      // Create the barycentric coordinate matrix.
      Matrix4fp Q;
      for(int j = 0; j < 4; j++) {
         Q.col(j) << xs[3 * inds[j]], xs[3 * inds[j] + 1], xs[3 * inds[j] + 2],
            1;
      }
      Matrix4fp Q1 = Q * Pinvs[i];
      
      // Perform the polar decomposition for the element to extract the
      // rotation.
      Matrix3fp Bm = Q1.block(0, 0, 3, 3);
      Eigen::SelfAdjointEigenSolver<Matrix3fp> eigs(Bm.transpose() * Bm);
      if(eigs.eigenvalues()[0] < 1e-10 || eigs.eigenvalues()[1] < 1e-10
         || eigs.eigenvalues()[2] < 1e-10) {
         R.setIdentity();
      } else {
         Matrix3fp U = 1 / sqrt(eigs.eigenvalues()[0])
            * eigs.eigenvectors().col(0)
            * eigs.eigenvectors().col(0).transpose()
            + 1 / sqrt(eigs.eigenvalues()[1])
            * eigs.eigenvectors().col(1)
            * eigs.eigenvectors().col(1).transpose()
            + 1 / sqrt(eigs.eigenvalues()[2])
            * eigs.eigenvectors().col(2)
            * eigs.eigenvectors().col(2).transpose();
         Matrix3fp Rm = Bm * U;
         
         // Set the global 12x12 rotation blocks.
         R.block(0, 0, 3, 3) = Rm;
         R.block(3, 3, 3, 3) = Rm;
         R.block(6, 6, 3, 3) = Rm;
         R.block(9, 9, 3, 3) = Rm;
      }
      
      // Transform the local stiffness matrix and shift vector.
      Matrix12fp currB = R * localBs[i] * R.transpose();
      Vector12fp currv = R * localvs[i];
      
      // Set the global stiffness elements and shift vectors.
      for(int j = 0; j < 4; j++) {
         int indj = constrained[inds[j]];
         
         if(indj >= 0) {
            for(int k = 0; k < 4; k++) {
               int indk = constrained[inds[k]];
               
               if(indk >= 0) {
                  for(int l1 = 0; l1 < 3; l1++) {
                     for(int l2 = 0; l2 < 3; l2++) {
                        Bts.push_back(Eigen::Triplet<fptype>(3 * indj + l1,
                           3 * indk + l2, currB(3 * j + l1, 3 * k + l2)));
                     }
                  }
               } else {
                  for(int l1 = 0; l1 < 3; l1++) {
                     for(int l2 = 0; l2 < 3; l2++) {
                        v[3 * indj + l1] -= currB(3 * j + l1, 3 * k + l2)
                           * xs[3 * inds[k] + l2];
                     }
                  }
               }
            }
            
            v[3 * indj] += currv[3 * j];
            v[3 * indj + 1] += currv[3 * j + 1];
            v[3 * indj + 2] += currv[3 * j + 2];
         }
      }
   }
   
   // Finally, assemble the matrices.
   B.setFromTriplets(Bts.begin(), Bts.end());
   D = -d_m * M - d_s * B;
}

void CorotatedLinearDeformable::assembleFactoredStiffness() {
   v.setZero();
   
   // Triplets used for sparse matrix initialization.
   std::vector<Eigen::Triplet<fptype> > Cts;
   
   Matrix12fp R;
   R.setZero();
   for(int i = 0; i < n_t; i++) {
      // Grab the node indices.
      int curr_ti = 4 * i;
      int inds[] = {elements[curr_ti], elements[curr_ti + 1],
         elements[curr_ti + 2], elements[curr_ti + 3]};
      
      // Create the barycentric coordinate matrix.
      Matrix4fp Q;
      for(int j = 0; j < 4; j++) {
         Q.col(j) << xs[3 * inds[j]], xs[3 * inds[j] + 1], xs[3 * inds[j] + 2],
            1;
      }
      Matrix4fp Q1 = Q * Pinvs[i];
      
      // Perform the polar decomposition for the element to extract the
      // rotation.
      Matrix3fp Bm = Q1.block(0, 0, 3, 3);
      Eigen::SelfAdjointEigenSolver<Matrix3fp> eigs(Bm.transpose() * Bm);
      if(eigs.eigenvalues()[0] < 1e-10 || eigs.eigenvalues()[1] < 1e-10
         || eigs.eigenvalues()[2] < 1e-10) {
         R.setIdentity();
      } else {
         Matrix3fp U = 1 / sqrt(eigs.eigenvalues()[0])
            * eigs.eigenvectors().col(0)
            * eigs.eigenvectors().col(0).transpose()
            + 1 / sqrt(eigs.eigenvalues()[1])
            * eigs.eigenvectors().col(1)
            * eigs.eigenvectors().col(1).transpose()
            + 1 / sqrt(eigs.eigenvalues()[2])
            * eigs.eigenvectors().col(2)
            * eigs.eigenvectors().col(2).transpose();
         Matrix3fp Rm = Bm * U;
         
         // Set the global 12x12 rotation blocks.
         R.block(0, 0, 3, 3) = Rm;
         R.block(3, 3, 3, 3) = Rm;
         R.block(6, 6, 3, 3) = Rm;
         R.block(9, 9, 3, 3) = Rm;
      }
      
      // Transform the local stiffness matrix and shift vector.
      Matrix6x12fp currC = localBsFactored[i] * R.transpose();
      Matrix12fp currB = currC.transpose() * currC;
      Vector12fp currv = R * localvs[i];
      
      // Set the global stiffness elements and shift vectors.
      for(int k = 0; k < 4; k++) {
         int indk = constrained[inds[k]];
         
         if(indk >= 0) {
            for(int j = 0; j < 6; j++) {
               for(int l = 0; l < 3; l++) {
                  Cts.push_back(Eigen::Triplet<fptype>(6 * i + j, 3 * indk + l,
                     currC(j, 3 * k + l)));
               }
            }
            
            v[3 * indk] += currv[3 * k];
            v[3 * indk + 1] += currv[3 * k + 1];
            v[3 * indk + 2] += currv[3 * k + 2];
         } else {
            for(int j = 0; j < 4; j++) {
               int indj = constrained[inds[j]];
               
               if(indj >= 0) {
                  for(int l1 = 0; l1 < 3; l1++) {
                     for(int l2 = 0; l2 < 3; l2++) {
                        v[3 * indj + l1] -= currB(3 * j + l1, 3 * k + l2)
                           * xs[3 * inds[k] + l2];
                     }
                  }
               }
            }
         }
      }
   }
   
   // Finally, assemble the matrix.
   C.setFromTriplets(Cts.begin(), Cts.end());
}
