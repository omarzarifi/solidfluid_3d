#include <ctime>

#include "array3_utils.h"
#include "fluidsim.h"
#include "openglutils.h"

#include "Eigen/IterativeLinearSolvers"
#include "Eigen/SparseCholesky"
#include "Eigen/SparseLU"

#include "Eigen/MarketIO.h"

#ifdef __APPLE__
#include <GLUT/glut.h> // why does Apple have to put glut.h here...
#else
#include <GL/glut.h> // ...when everyone else puts it here?
#endif

/*****************************************************************************
 *                         SIMULATION CONSTANTS                              *
 *****************************************************************************/

// This constant governs how far away from the walls to project a trespassing
// particle (ex: 1.02 means that there will be a 0.02*dx distance between the
// particle and the wall at the end.
const static fptype FRACTION = 1.25;

// Error tolerance for iterative linear solvers.
const static fptype ITERATIVE_TOLERANCE = 1e-10;

// Minimum time step size.
const static fptype MIN_TIMESTEP = 1e-6;
// Minimum face area (anything below this gets set to 0).
const static fptype MIN_FACE_AREA = 0.02;

// Parameters for static SPD conversion. SHIFT_AMOUNT is always static.
const static fptype SHIFT_AMOUNT = 0.9;
const static fptype K_MULTIPLIER = -0.002;

// Parameters for dynamic SPD conversion. Gives the range for K_MULTIPLIER.
const static fptype MIN_K_MULTIPLIER = 1e-5;
const static fptype MAX_K_MULTIPLIER = 10;

/*****************************************************************************
 *                         SIMULATION CONSTANTS                              *
 *****************************************************************************/

// The deformable that MINRESPreconditioner will use.
Deformable* MINRES_DEF;

class MINRESPreconditioner {

private:
   
   // Size of the total system.
   int total_size;
   // Size of the pressure block.
   int np;
   // Size of the solid block.
   int nv;
   
   // Pressure block for the system.
   SparseMatrixXfp block1;
   // Preconditioner for the pressure block.
   Eigen::IncompleteCholesky<fptype> block1_pc;
   // Solution.
   VectorXfp* solution;
   
public:
   
   MINRESPreconditioner() {
      
   }
   
   explicit MINRESPreconditioner(const SparseMatrixXfp& matrix) {
      compute(matrix);
   }
   
   MINRESPreconditioner& analyzePattern(const SparseMatrixXfp& matrix) {
      // Form the pressure block.
      total_size = matrix.cols();
      nv = 3 * (MINRES_DEF->n_v - MINRES_DEF->n_constrained);
      np = total_size - nv;
      std::vector<Eigen::Triplet<fptype> > block1_entries;
      for(int i = 0; i < matrix.outerSize(); i++) {
         for(SparseMatrixXfp::InnerIterator it(matrix, i); it; ++it) {
            if(it.row() < np && it.col() < np) {
               block1_entries.push_back(Eigen::Triplet<fptype>(it.row(),
                  it.col(), it.value()));
            }
         }
      }
      block1.resize(np, np);
      block1.setFromTriplets(block1_entries.begin(), block1_entries.end());
      
      // Form the incomplete Cholesky factorization of the pressure block.
      block1_pc.analyzePattern(block1);
      
      // Allocate the solution vector.
      solution = new VectorXfp(total_size);
      
      return *this;
   }
   
   MINRESPreconditioner& factorize(const SparseMatrixXfp& matrix) {
      block1_pc.factorize(block1);
      
      return *this;
   }
   
   MINRESPreconditioner& compute(const SparseMatrixXfp& matrix) {
      return analyzePattern(matrix).factorize(matrix);
   }
   
   inline const VectorXfp& solve(const VectorXfp& rhs) const {
      solution->head(np) = block1_pc.solve(rhs.head(np));
      solution->tail(nv) = MINRES_DEF->Minv * rhs.tail(nv);
      
      return *solution;
   }
   
   ~MINRESPreconditioner() {
      delete solution;
   }
   
   Eigen::ComputationInfo info() {
      return block1_pc.info();
   }
   
};

/*****************************************************************************
 *                           HELPER FUNCTIONS                                *
 *****************************************************************************/

/**
 * Given two signed distance values, determine what fraction of a connecting
 * segment is "inside".
 */
fptype fraction_inside(fptype phi_left, fptype phi_right) {
   if(phi_left < 0 && phi_right < 0) {
      return 1;
   }
   if (phi_left < 0 && phi_right >= 0) {
      return phi_left / (phi_left - phi_right);
   }
   if(phi_left >= 0 && phi_right < 0) {
      return phi_right / (phi_right - phi_left);
   }

   return 0;
}

/**
 * Circularly cycles the values in the array by one to the left.
 */
void cycle_array(fptype* arr, int size) {
   fptype t = arr[0];
   for(int i = 0; i < size - 1; i++) {
      arr[i] = arr[i+1];
   }
   arr[size - 1] = t;
}

/**
 * Given four signed distance values (at the corners of a square), determine
 * what fraction of the square is "inside".
 */
fptype fraction_inside(fptype phi_bl, fptype phi_br, fptype phi_tl,
   fptype phi_tr) {
   int inside_count = (phi_bl < 0 ? 1 : 0) + (phi_tl < 0 ? 1 : 0)
      + (phi_br < 0 ? 1 : 0) + (phi_tr < 0 ? 1 : 0);
   fptype list[] = {phi_bl, phi_br, phi_tr, phi_tl};

   if(inside_count == 4) {
      return 1;
   } else if (inside_count == 3) {
      // Rotate until the positive value is in the first position.
      while(list[0] < 0) {
         cycle_array(list, 4);
      }
      
      // Work out the area of the exterior triangle
      fptype side0 = 1 - fraction_inside(list[0], list[3]);
      fptype side1 = 1 - fraction_inside(list[0], list[1]);
      return 1 - 0.5 * side0 * side1;
   } else if(inside_count == 2) {
      // Rotate until a negative value is in the first position, and the next
      // negative is in either slot 1 or 2.
      while(list[0] >= 0 || !(list[1] < 0 || list[2] < 0)) {
         cycle_array(list, 4);
      } 
      
      if(list[1] < 0) { // the matching signs are adjacent
         fptype side_left = fraction_inside(list[0], list[3]);
         fptype side_right = fraction_inside(list[1], list[2]);
         return 0.5 * (side_left + side_right);
      } else { // matching signs are diagonally opposite
         // Determine the centre point's sign to disambiguate this case.
         fptype middle_point = list[0] + list[1] + list[2] + list[3];
         if(middle_point < 0) {
            fptype area = 0;

            // First triangle (top left)...
            fptype side1 = 1 - fraction_inside(list[0], list[3]);
            fptype side3 = 1 - fraction_inside(list[2], list[3]);

            area += 0.5 * side1 * side3;

            // Second triangle (top right)...
            fptype side2 = 1 - fraction_inside(list[2], list[1]);
            fptype side0 = 1 - fraction_inside(list[0], list[1]);
            area += 0.5 * side0 * side2;
            
            return 1 - area;
         } else {
            fptype area = 0;

            // First triangle (bottom left)...
            fptype side0 = fraction_inside(list[0], list[1]);
            fptype side1 = fraction_inside(list[0], list[3]);
            area += 0.5 * side0 * side1;

            // Second triangle (top right)...
            fptype side2 = fraction_inside(list[2], list[1]);
            fptype side3 = fraction_inside(list[2], list[3]);
            area += 0.5 * side2 * side3;
            return area;
         }
         
      }
   } else if(inside_count == 1) {
      // Rotate until the negative value is in the first position
      while(list[0] >= 0) {
         cycle_array(list, 4);
      }

      // Work out the area of the interior triangle, and subtract from 1.
      fptype side0 = fraction_inside(list[0], list[3]);
      fptype side1 = fraction_inside(list[0], list[1]);
      return 0.5 * side0 * side1;
   } else {
      return 0;
   }
}

/**
 * This function propagates the valid values of the given grid to their invalid
 * neighbours.
 */
void extrapolate(Array3fp& grid, Array3fp& valid) {
   Array3fp old_grid = grid;
   Array3fp old_valid = valid;
   int ni = grid.ni;
   int nj = grid.nj;
   int nk = grid.nk;
   
   for(int k = 0; k < nk; k++) {
      for(int j = 0; j < nj; j++) {
         for(int i = 0; i < ni; i++) {
            if(old_valid(i, j, k) == 0) {
               fptype sum = 0;
               int count = 0;
               
               if(i < ni - 1 && old_valid(i + 1, j, k) > 0) {
                  sum += old_grid(i + 1, j, k);
                  count++;
               }
               if(i > 0 && old_valid(i - 1, j, k) > 0) {
                  sum += old_grid(i - 1, j, k);
                  count++;
               }
               if(j < nj - 1 && old_valid(i, j + 1, k) > 0) {
                  sum += old_grid(i, j + 1, k);
                  count++;
               }
               if(j > 0 && old_valid(i, j - 1, k) > 0) {
                  sum += old_grid(i, j - 1, k);
                  count++;
               }
               if(k < nk - 1 && old_valid(i, j, k + 1) > 0) {
                  sum += old_grid(i, j, k + 1);
                  count++;
               }
               if(k > 0 && old_valid(i, j, k - 1) > 0) {
                  sum += old_grid(i, j, k - 1);
                  count++;
               }
               
               if(count > 0) {
                  grid(i, j, k) = sum / count;
                  valid(i, j, k) = 1;
               }
            }
         }
      }
   }
}

/**
 * This function zeros out the invalid values of the given grid.
 */
void zeroOut(Array3fp& grid, const Array3fp& valid) {
   #pragma omp parallel for
   for(int k = 0; k < grid.nk; k++) {
      for(int j = 0; j < grid.nj; j++) {
         for(int i = 0; i < grid.ni; i++) {
            if(valid(i, j, k) == 0) {
               grid(i, j, k) = 0;
            }
         }
      }
   }
}

/**
 * Returns true if at least one of the arguments is positive and at least one
 * is negative and the average of the eight arguments is negative; returns
 * false otherwise.
 */
bool posAndNeg_avg(double a, double b, double c, double d, double e, double f,
   double g, double h) {
   bool pos = a > 0 || b > 0 || c > 0 || d > 0 || e > 0 || f > 0 || g > 0
      || h > 0;
   bool neg = a < 0 || b < 0 || c < 0 || d < 0 || e < 0 || f < 0 || g < 0
      || h < 0;
   bool avg = (a + b + c + d + e + f + g + h) < 0;
   return pos && neg && avg;
}

/*****************************************************************************
 *                                  FLUID                                    *
 *****************************************************************************/

Fluid::Fluid() {
   
}

void Fluid::initialize(double rho_, int ni_, double pr_) {
   rho = rho_;
   ni = ni_;
   dx = 1.0 / ni;
   particle_radius = pr_;
   
   us.resize(ni, ni + 1, ni);
   us.assign(0);
   temp_us.resize(ni, ni + 1, ni);
   vs.resize(ni + 1, ni, ni);
   vs.assign(0);
   temp_vs.resize(ni + 1, ni, ni);
   ws.resize(ni, ni, ni + 1);
   ws.assign(0);
   temp_ws.resize(ni, ni, ni + 1);
   
   F.resize(ni, ni, ni);
   F.assign(-2);
   Fu.resize(ni, ni + 1, ni);
   Fv.resize(ni + 1, ni, ni);
   Fw.resize(ni, ni, ni + 1);
   
   liquid_phi.resize(ni, ni, ni);
   solid_phi.resize(ni + 1, ni + 1, ni + 1);
   
   // Initialize the solid signed distance field.
   solid_phi.assign(dx);
   
   def = NULL;
   g[0] = 0;
   g[1] = 0;
   g[2] = 0;
}

void Fluid::advance(fptype dt) {
   fptype t = 0;
   clock_t timer;
   clock_t time0;
   
   while(t < dt) {
      time0 = clock();
      
      fptype substep = use_CFL ? min(cfl(), dt - t) : dt;
      if(info_stream.is_open()) {
         info_stream << substep << "\t";
      }
      
      // Move the solid with its current velocities.
      if(def != NULL) {
         def->move(substep);
         
         timer = clock();
         def->computeSolidPhi(dx, solid_phi);
         if(info_stream.is_open()) {
            info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
         }
         
         timer = clock();
         if(projection_method != PPM_FORCE_JACOBIAN_SPD_FAC2) {
            def->assembleStiffness();
         } else {
            def->assembleFactoredStiffness();
         }
         if(info_stream.is_open()) {
            info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
         }
      }
      
      timer = clock();
      // Advect the particles.
      advectParticles(substep);
      // Advance the velocity field.
      advect(substep);
      if(info_stream.is_open()) {
         info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
      }
      
      // Apply gravity first.
      addAcceleration(g, substep);
      if(def != NULL && solid_gravity) {
         def->addAcceleration(g, substep);
      }
      
      // Perform pressure projection.
      timer = clock();
      pProject(substep);
      if(info_stream.is_open()) {
         info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
      }
      
      // Extrapolate the velocity fields.
      timer = clock();
      setValid();
      for(int i = 0; i < 10; i++) {
         extrapolate(us, Fu);
         extrapolate(vs, Fv);
         extrapolate(ws, Fw);
      }
      zeroOut(us, Fu);
      zeroOut(vs, Fv);
      zeroOut(ws, Fw);
      
      // Constrain velocities extrapolated inside the solid.
      if(def != NULL) {
         constrainVelocities();
      }
      
      if(info_stream.is_open()) {
         info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
      }
      
      t += substep;
      
      if(info_stream.is_open()) {
         info_stream << (double) (clock() - time0) / CLOCKS_PER_SEC
            << std::endl;
      }
   }
}

void Fluid::addAcceleration(fptype a[3], fptype dt) {
   fptype au = a[1] * dt;
   fptype av = a[0] * dt;
   fptype aw = a[2] * dt;
   
   #pragma omp parallel for
   for(int i = 0; i < us.a.size(); i++) {
      us.a[i] += au;
      vs.a[i] += av;
      ws.a[i] += aw;
   }
}

void Fluid::addAccelerationAbove(fptype a[3], fptype dt, fptype y) {
   fptype au = a[1] * dt;
   fptype av = a[0] * dt;
   fptype aw = a[2] * dt;
   
   #pragma omp parallel for
   for(int xc = 0; xc < ni; xc++) {
      for(int yc = std::ceil(y / dx); yc < ni; yc++) {
         for(int zc = 0; zc < ni; zc++) {
            us(xc, yc, zc) += au;
            vs(xc, yc, zc) += av;
            ws(xc, yc, zc) += aw;
         }
      }
   }
}

void Fluid::addParticle(const Vec3fp& position) {
   particles.push_back(position);
}

void Fluid::setGravity(fptype gx, fptype gy, fptype gz) {
   g[0] = gx;
   g[1] = gy;
   g[2] = gz;
}

void Fluid::setDeformable(Deformable* def_) {
   MINRES_DEF = def_;
   def = def_;
   
   if(export_mat) {
      Eigen::saveMarket(def->M, "matlab/M.mat");
   }
}

void Fluid::render() const {
   glColor3f(0, 0, 1);
   Vec3fp centre;
   for(int i = 0; i < particles.size(); i++) {
      centre[0] = particles[i][0];
      centre[1] = particles[i][1];
      centre[2] = particles[i][2];
      draw_circle2d(centre, particle_radius, 20);
   }
}

void Fluid::setProjectionMethod(int method) {
   projection_method = method;
}

void Fluid::setIterativeSolve(bool a) {
   iterative_solve = a;
}

void Fluid::setUseCFL(bool a) {
   use_CFL = a;
}

void Fluid::writeToMatlab(bool a) {
   export_mat = a;
}

bool Fluid::setInfoStream(const char* file) {
   if(info_stream.is_open()) {
      info_stream.close();
   }
   
   info_stream.open(file);
   return info_stream.is_open();
}

void Fluid::addSphere(const Vec3fp& c, float r, int seed, int perCell) {
   double ps = 8 * r * r * r / (dx * dx * dx) * perCell;
   
   for(int i = seed; i < ps + seed; i++) {
      fptype x = randhashd(3 * i) * 2 * r - r;
      fptype y = randhashd(3 * i + 1) * 2 * r - r;
      fptype z = randhashd(3 * i + 2) * 2 * r - r;
      Vec3fp p(x, y, z);
      if(mag(p) < r) {
         addParticle(c + p);
      }
   }
}

fptype Fluid::cfl() {
   fptype maxvel = def == NULL ? 0 : def->maxV();
   
   for(int i = 0; i < us.a.size(); i++) {
      maxvel = max(maxvel, fabs(us.a[i]));
   }
   for(int i = 0; i < vs.a.size(); i++) {
      maxvel = max(maxvel, fabs(vs.a[i]));
   }
   for(int i = 0; i < ws.a.size(); i++) {
      maxvel = max(maxvel, fabs(ws.a[i]));
   }
   
   maxvel += sqrt(25 * dx);
   return max(MIN_TIMESTEP, dx / maxvel);
}

void Fluid::advectParticles(fptype dt) {
   for(int i = 0; i < particles.size(); i++) {
      Vec3fp pos = particles[i];
      pos += dt / 2 * getVelocity(pos);
      particles[i] += dt * getVelocity(pos);
      
      // Project the particle out of the walls if necessary.
      if(particles[i][0] < dx * FRACTION) {
         particles[i][0] = dx * FRACTION;
      } else if(particles[i][0] > 1 - dx * FRACTION) {
         particles[i][0] = 1 - dx * FRACTION;
      }
      if(particles[i][1] < dx * FRACTION) {
         particles[i][1] = dx * FRACTION;
      } else if(particles[i][1] > 1 - dx * FRACTION) {
         particles[i][1] = 1 - dx * FRACTION;
      }
      if(particles[i][2] < dx * FRACTION) {
         particles[i][2] = dx * FRACTION;
      } else if(particles[i][2] > 1 - dx * FRACTION) {
         particles[i][2] = 1 - dx * FRACTION;
      }
      
      // Project the particles out of the dynamic solid if necessary.
      if(def != NULL) {
         fptype phi = interpolate_value(particles[i] / dx, solid_phi);
         if(phi < (FRACTION - 1) * dx) {
            Vec3fp normal;
            interpolate_gradient(normal, particles[i] / dx, solid_phi);
            normalize(normal);
            particles[i] += ((FRACTION - 1) * dx - phi) * normal;
         }
      }
   }
}

Vec3fp Fluid::traceRK2(Vec3fp &position, fptype dt) {
   Vec3fp velocity = getVelocity(position);
   velocity = getVelocity(position + dt / 2 * velocity);
   return position + dt * velocity;
}

void Fluid::advect(fptype dt) {
   // Perform semi-Lagrangian advection for u and v velocities.
   for(int k = 0; k <= ni; k++) {
      for(int j = 0; j <= ni; j++) {
         for(int i = 0; i <= ni; i++) {
            // Advection of u velocities...
            if(i != ni && k != ni) {
               Vec3fp pos((i + 0.5) * dx, j * dx, (k + 0.5) * dx);
               pos = traceRK2(pos, -dt);
               temp_us(i, j, k) = getVelocity(pos)[1];
            }
            
            // Advection of v velocities.
            if(j != ni && k != ni) {
               Vec3fp pos(i * dx, (j + 0.5) * dx, (k + 0.5) * dx);
               pos = traceRK2(pos, -dt);
               temp_vs(i, j, k) = getVelocity(pos)[0];
            }
            
            // Advection of w velocities.
            if(i != ni && j != ni) {
               Vec3fp pos((i + 0.5) * dx, (j + 0.5) * dx, k * dx);
               pos = traceRK2(pos, -dt);
               temp_ws(i, j, k) = getVelocity(pos)[2];
            }
         }
      }
   }
   
   us = temp_us;
   vs = temp_vs;
   ws = temp_ws;
}

/*void applyViscosity(fptype dt) {
   for(int k = 1; k < ni; k++) {
      for(int j = 1; j < ni; j++) {
         for(int i = 1; i < ni; i++) {
            temp_us(i, j, k)
         }
      }
   }
}

fptype findViscosityCorrection(Array3fp u, const Array3fp valid, int i, int j,
   int k) {
   int neighbours = 0;
   fptype total = 0;
   
   if(valid(i, j, k) > 0) {
      if(valid(i - 1, j, k) > 0) {
         neighbours++;
         total += u(i - 1, j, k);
      }
      if(valid(i + 1, j, k) > 0) {
         neighbours++;
         total += u(i + 1, j, k);
      }
      if(valid(i, j - 1, k) > 0) {
         neighbours++;
         total += u(i, j - 1, k);
      }
      if(valid(i, j + 1, k) > 0) {
         neighbours++;
         total += u(i, j + 1, k);
      }
      if(valid(i, j, k - 1) > 0) {
         neighbours++;
         total += u(i, j, k - 1);
      }
      if(valid(i, j, k + 1) > 0) {
         neighbours++;
         total += u(i, j, k + 1);
      }
   }
   
   return total - neighbours * u(i, j, k);
}*/

int Fluid::setF() {
   int nc = 0;
   
   // Identify the active cells.
   for(int k = 1; k < ni - 1; k++) {
      for(int j = 1; j < ni - 1; j++) {
         for(int i = 1; i < ni - 1; i++) {
            if(liquid_phi(i, j, k) <= 0 && (Fu(i, j, k) > 0
               || Fu(i, j + 1, k) > 0 || Fv(i, j, k) > 0 || Fv(i + 1, j, k) > 0
               || Fw(i, j, k) > 0 || Fw(i, j, k + 1))) {
               F(i, j, k) = nc++;
            } else if(solid_phi(i, j, k) < 0 && solid_phi(i + 1, j, k) < 0
               && solid_phi(i, j + 1, k) < 0 && solid_phi(i + 1, j + 1, k) < 0
               && solid_phi(i, j, k + 1) < 0 && solid_phi(i + 1, j, k + 1) < 0
               && solid_phi(i, j + 1, k + 1) < 0
               && solid_phi(i + 1, j + 1, k + 1) < 0) {
               F(i, j, k) = -4;
            } else {
               F(i, j, k) = -1;
            }
         }
      }
   }
   
   if(info_stream.is_open()) {
      info_stream << nc << "\t";
   }
   
   return nc;
}

void Fluid::setFAreas() {
   clock_t timer = clock();
   
   for(int k = 1; k < ni; k++) {
      for(int j = 1; j < ni; j++) {
         for(int i = 1; i < ni; i++) {
            fptype fraction = fraction_inside(solid_phi(i, j, k),
               solid_phi(i + 1, j, k), solid_phi(i, j, k + 1),
               solid_phi(i + 1, j, k + 1));
            if(fraction < MIN_FACE_AREA) {
               fraction = 0;
            } else if(fraction > 1 - MIN_FACE_AREA) {
               fraction = 1;
            }
            Fu(i, j, k) = (1 - fraction) * dx * dx;
            
            fraction = fraction_inside(solid_phi(i, j, k),
               solid_phi(i, j, k + 1), solid_phi(i, j + 1, k),
               solid_phi(i, j + 1, k + 1));
            if(fraction < MIN_FACE_AREA) {
               fraction = 0;
            } else if(fraction > 1 - MIN_FACE_AREA) {
               fraction = 1;
            }
            Fv(i, j, k) = (1 - fraction) * dx * dx;
            
            fraction = fraction_inside(solid_phi(i, j, k),
               solid_phi(i + 1, j, k), solid_phi(i, j + 1, k),
               solid_phi(i + 1, j + 1, k));
            if(fraction < MIN_FACE_AREA) {
               fraction = 0;
            } else if(fraction > 1 - MIN_FACE_AREA) {
               fraction = 1;
            }
            Fw(i, j, k) = (1 - fraction) * dx * dx;
         }
      }
   }
   
   for(int i = 0; i < ni; i++) {
      for(int j = 0; j < ni; j++) {
         Fu(i, 0, j) = 0;
         Fu(i, ni, j) = 0;
         Fu(0, i, j) = 0;
         Fu(ni - 1, i, j) = 0;
         Fu(i, j, 0) = 0;
         Fu(i, j, ni - 1) = 0;
         
         Fv(0, i, j) = 0;
         Fv(ni, i, j) = 0;
         Fv(i, 0, j) = 0;
         Fv(i, ni - 1, j) = 0;
         Fv(i, j, 0) = 0;
         Fv(i, j, ni - 1) = 0;
         
         Fw(i, j, 0) = 0;
         Fw(i, j, ni) = 0;
         Fw(i, 0, j) = 0;
         Fw(i, ni - 1, j) = 0;
         Fw(0, i, j) = 0;
         Fw(ni - 1, i, j) = 0;
      }
   }
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
}

void Fluid::computePhi() {
   clock_t timer = clock();
   
   // First compute the grid liquid distance field.
   liquid_phi.assign(3 * dx);
   for(int c = 0; c < particles.size(); c++) {
      Vec3fp p = particles[c];
      // Find the containing cell for the particle.
      int i = std::floor(p[0] / dx);
      int j = std::floor(p[1] / dx);
      int k = std::floor(p[2] / dx);
      
      // Compute distance to surrounding nodes and set the distance if
      // appropriate.
      for(int k_off = k - 3; k_off <= k + 3; k_off++) {
         for(int j_off = j - 3; j_off <= j + 3; j_off++) {
            for(int i_off = i - 3; i_off <= i + 3; i_off++) {
               if(i_off >= 0 && i_off < ni && j_off >= 0 && j_off < ni
                  && k_off >= 0 && k_off < ni) {
                  // Do distance computations for cell samples.
                  Vec3fp node((i_off + 0.5) * dx, (j_off + 0.5) * dx,
                     (k_off + 0.5) * dx);
                  fptype distance = dist(p, node) - particle_radius;
                  liquid_phi(i_off, j_off, k_off) = min(distance,
                     liquid_phi(i_off, j_off, k_off));
               }
            }
         }
      }
   }
   
   // Extend the fluid region into boundary areas of the solid.
   if(def != NULL) {
      for(int k = 1; k < ni - 1; k++) {
         for(int j = 1; j < ni - 1; j++) {
            for(int i = 1; i < ni - 1; i++) {
               if(liquid_phi(i, j, k) < dx / 2
                  && posAndNeg_avg(solid_phi(i, j, k), solid_phi(i + 1, j, k),
                  solid_phi(i, j + 1, k), solid_phi(i + 1, j + 1, k),
                  solid_phi(i, j, k + 1), solid_phi(i + 1, j, k + 1),
                  solid_phi(i, j + 1, k + 1), solid_phi(i + 1, j + 1, k + 1))) {
                  liquid_phi(i, j, k) = -dx / 2;
               }
            }
         }
      }
   }
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
}

Vec3fp Fluid::getVelocity(const Vec3fp& position) const {
   //Interpolate the velocity from the u and v grids.
   fptype u_value = interpolate_value(position / dx - Vec3fp(0.5, 0, 0.5), us);
   fptype v_value = interpolate_value(position / dx - Vec3fp(0, 0.5, 0.5), vs);
   fptype w_value = interpolate_value(position / dx - Vec3fp(0.5, 0.5, 0), ws);
   
   return Vec3fp(v_value, u_value, w_value);
}

fptype Fluid::getTheta(int i, int j, int k, int i_off, int j_off, int k_off) {
   fptype phi0 = liquid_phi(i, j, k);
   fptype phi1 = liquid_phi(i + i_off, j + j_off, k + k_off);
   
   return max(fraction_inside(phi0, phi1), 0.01);
}

void Fluid::pProject(fptype dt) {
   if(projection_method == PPM_FORCE_JACOBIAN) {
      pProject_v1(dt);
   } else if(projection_method == PPM_FORCE_JACOBIAN_SPD) {
      if(def == NULL) {
         std::cout << "The chosen projection scheme requires a deformable body"
            << " to be coupled.\n";
         exit(0);
      } else {
         pProject_v2(dt);
      }
   } else if(projection_method == PPM_FORCE_JACOBIAN_SPD_DYN) {
      if(def == NULL) {
         std::cout << "The chosen projection scheme requires a deformable body"
            << " to be coupled.\n";
         exit(0);
      } else {
         pProject_v2b(dt);
      }
   } else if(projection_method == PPM_FORCE_JACOBIAN_DAMPING) {
      pProject_v4(dt);
   } else if(projection_method == PPM_DECOUPLED) {
      pProject_v3(dt);
   } else if(projection_method == PPM_FORCE_JACOBIAN_SPD_FAC) {
      if(def == NULL) {
         std::cout << "The chosen projection scheme requires a deformable body"
            << " to be coupled.\n";
         exit(0);
      } else {
         pProject_v5(dt);
      }
   } else if(projection_method == PPM_FORCE_JACOBIAN_SPD_FAC2) {
      if(def == NULL) {
         std::cout << "The chosen projection scheme requires a deformable body"
            << " to be coupled.\n";
         exit(0);
      } else {
         pProject_v6(dt);
      }
   } else {
      std::cout << "Undefined projection scheme...\n";
      exit(0);
   }
}

void Fluid::pProject_v1(fptype dt) {
   clock_t timer;
   
   computePhi();
   setFAreas();
   int np = setF();
   if(def != NULL) {
      timer = clock();
      def->intersectGrid(boundary, dx, F);
      if(info_stream.is_open()) {
         info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t"
            << boundary.size() << "\t";
      }
   }
   int nv = def == NULL ? 0 : 3 * (def->n_v - def->n_constrained);
   
   std::vector<Eigen::Triplet<fptype> > Mts;
   SparseMatrixXfp M(np + nv, np + nv);
   VectorXfp rhs(np + nv);
   
   timer = clock();
   // Generate the linear system. This first loop takes care of the divergence-
   // free condition in regular cells.
   fptype alpha = dt / (rho * dx);
   for(int k = 1; k < ni - 1; k++) {
      for(int j = 1; j < ni - 1; j++) {
         for(int i = 1; i < ni - 1; i++) {
            int c_ind = F(i, j, k);
            
            if(c_ind < 0) {
               continue;
            }
            rhs[c_ind] = 0;
            
            // Take care of the left neighbour.
            int n_ind = F(i - 1, j, k);
            fptype term = Fv(i, j, k) * alpha;
            fptype rterm = Fv(i, j, k) * vs(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, -1, 0, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the right neighbour.
            n_ind = F(i + 1, j, k);
            term = Fv(i + 1, j, k) * alpha;
            rterm = Fv(i + 1, j, k) * vs(i + 1, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 1, 0, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the bottom neighbour.
            n_ind = F(i, j - 1, k);
            term = Fu(i, j, k) * alpha;
            rterm = Fu(i, j, k) * us(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, -1, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the top neighbour.
            n_ind = F(i, j + 1, k);
            term = Fu(i, j + 1, k) * alpha;
            rterm = Fu(i, j + 1, k) * us(i, j + 1, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 1, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the back neighbour.
            n_ind = F(i, j, k - 1);
            term = Fw(i, j, k) * alpha;
            rterm = Fw(i, j, k) * ws(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, -1)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the front neighbour.
            n_ind = F(i, j, k + 1);
            term = Fw(i, j, k + 1) * alpha;
            rterm = Fw(i, j, k + 1) * ws(i, j, k + 1);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, 1)));
               rhs[c_ind] -= rterm;
            }
         }
      }
   }
   
   // Computes the force Jacobian for the solid.
   for(int i = 0; i < boundary.size(); i++) {
      BoundarySample& bs = boundary[i];
      
      for(int node = 0; node < 3; node++) {
         if(bs.n_index[node] >= 0) {
            int nindex = np + 3 * bs.n_index[node];
         
            for(int dim = 0; dim < 3; dim++) {
               Mts.push_back(Eigen::Triplet<fptype>(nindex + dim, bs.p_ind,
                  bs.barc[node] * bs.n[dim]));
               Mts.push_back(Eigen::Triplet<fptype>(bs.p_ind, nindex + dim,
                  bs.barc[node] * bs.n[dim]));
            }
         }
      }
   }
   
   // Take care of solid damping and elasticity.
   if(def != NULL) {
      SparseMatrixXfp solid = def->M / dt + dt * def->B - def->D;
      for(int i = 0; i < solid.outerSize(); i++) {
         for(SparseMatrixXfp::InnerIterator it(solid, i); it; ++it) {
            Mts.push_back(Eigen::Triplet<fptype>(np + it.row(), np + it.col(),
               -it.value()));
         }
      }
      
      rhs.tail(nv) = -(def->M * def->vs2 / dt - def->B * def->xs2 + def->v);
   }
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
   
   // Solve the linear system.
   M.setFromTriplets(Mts.begin(), Mts.end());
   if(info_stream.is_open()) {
      info_stream << M.nonZeros() << "\t";
   }
   Eigen::VectorXd p;
   timer = clock();
   if(iterative_solve) {
      Eigen::BiCGSTAB<SparseMatrixXfp, Eigen::IncompleteLUT<fptype> > solver;
      //Eigen::MINRES<SparseMatrixXfp, Eigen::Lower | Eigen::Upper, MINRESPreconditioner> solver;
      solver.setTolerance(ITERATIVE_TOLERANCE);
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         exit(0);
      }
      
      Eigen::VectorXd p0(np + nv);
      p0.setZero();
      if(def != NULL) {
         p0.tail(nv) = def->vs2;
      }
      p = solver.solveWithGuess(rhs, p0);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         std::cout << "Error: " << solver.error() << std::endl;
         exit(0);
      }
      
      if(info_stream.is_open()) {
         info_stream << solver.iterations() << "\t";
      }
   } else {
      Eigen::SparseLU<Eigen::SparseMatrix<fptype> > solver;
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         exit(0);
      }
      
      p = solver.solve(rhs);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         exit(0);
      }
   }
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
   
   // Update the velocity field.
   timer = clock();
   for(int k = 1; k < ni; k++) {
      for(int j = 1; j < ni; j++) {
         for(int i = 1; i < ni; i++) {
            fptype p_n, p_p;
            
            // Update the y velocity component first.
            if(Fu(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(j == 1 || j == ni - 1) {
                  us(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j - 1, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) >= 0 && F(i, j - 1, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j - 1, k);
                  } else {
                     std::cout << "WTF [u]" << std::endl;
                  }
                  
                  us(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the x velocity component.
            if(Fv(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(i == 1 || i == ni - 1) {
                  vs(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i - 1, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) >= 0 && F(i - 1, j, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i - 1, j, k);
                  } else {
                     std::cout << "WTF [v]" << std::endl;
                  }
                  
                  vs(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the z velocity component.
            if(Fw(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(k == 1 || k == ni - 1) {
                  ws(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k - 1)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) >= 0 && F(i, j, k - 1) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j, k - 1);
                  } else {
                     std::cout << "WTF [w]" << std::endl;
                  }
                  
                  ws(i, j, k) -= alpha * (p_n - p_p);
               }
            }
         }
      }
   }
   
   // Copy the solid velocity data.
   if(def != NULL) {
      def->vs2 = p.tail(nv);
      
      for(int i = 0; i < def->n_v; i++) {
         if(def->constrained[i] >= 0) {
            def->vs[3 * i] = def->vs2[3 * def->constrained[i]];
            def->vs[3 * i + 1] = def->vs2[3 * def->constrained[i] + 1];
            def->vs[3 * i + 2] = def->vs2[3 * def->constrained[i] + 2];
         }
      }
   }
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
   
   // Write out the matrix to file if necessary.
   if(export_mat) {
      std::stringstream ssm;
      ssm << "matlab/A" << ++timestep_c << ".mat";
      Eigen::saveMarket(M, ssm.str());
      
      std::stringstream ssv;
      ssv << "matlab/b" << timestep_c << ".mat";
      Eigen::saveMarketVector(rhs, ssv.str());
   }
}

void Fluid::pProject_v2(double dt) {
   clock_t timer;
   
   computePhi();
   setFAreas();
   int np = setF();
   timer = clock();
   def->intersectGrid(boundary, dx, F, MIN_FACE_AREA * dx * dx / 2);
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t"
         << boundary.size() << "\t";
   }
   int nv = 3 * (def->n_v - def->n_constrained);
   
   std::vector<Eigen::Triplet<fptype> > Mts;
   SparseMatrixXfp M(np + nv, np + nv);
   VectorXfp rhs(np + nv);
   
   timer = clock();
   // Generate the linear system. This first loop takes care of the divergence-
   // free condition in regular cells.
   fptype alpha = dt / (rho * dx);
   for(int k = 1; k < ni - 1; k++) {
      for(int j = 1; j < ni - 1; j++) {
         for(int i = 1; i < ni - 1; i++) {
            int c_ind = F(i, j, k);
            
            if(c_ind < 0) {
               continue;
            }
            rhs[c_ind] = 0;
            
            // Take care of the left neighbour.
            int n_ind = F(i - 1, j, k);
            fptype term = Fv(i, j, k) * alpha;
            fptype rterm = Fv(i, j, k) * vs(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, -1, 0, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the right neighbour.
            n_ind = F(i + 1, j, k);
            term = Fv(i + 1, j, k) * alpha;
            rterm = Fv(i + 1, j, k) * vs(i + 1, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 1, 0, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the bottom neighbour.
            n_ind = F(i, j - 1, k);
            term = Fu(i, j, k) * alpha;
            rterm = Fu(i, j, k) * us(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, -1, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the top neighbour.
            n_ind = F(i, j + 1, k);
            term = Fu(i, j + 1, k) * alpha;
            rterm = Fu(i, j + 1, k) * us(i, j + 1, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 1, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the back neighbour.
            n_ind = F(i, j, k - 1);
            term = Fw(i, j, k) * alpha;
            rterm = Fw(i, j, k) * ws(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, -1)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the front neighbour.
            n_ind = F(i, j, k + 1);
            term = Fw(i, j, k + 1) * alpha;
            rterm = Fw(i, j, k + 1) * ws(i, j, k + 1);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, 1)));
               rhs[c_ind] -= rterm;
            }
         }
      }
   }
   
   // Computes the force Jacobian for the solid.
   std::vector<Eigen::Triplet<fptype> > Jts;
   SparseMatrixXfp J(nv, np);
   for(int i = 0; i < boundary.size(); i++) {
      BoundarySample& bs = boundary[i];
      
      for(int node = 0; node < 3; node++) {
         if(bs.n_index[node] >= 0) {
            int nindex = 3 * bs.n_index[node];
         
            for(int dim = 0; dim < 3; dim++) {
               Jts.push_back(Eigen::Triplet<fptype>(nindex + dim, bs.p_ind,
                  bs.barc[node] * bs.n[dim]));
            }
         }
      }
   }
   J.setFromTriplets(Jts.begin(), Jts.end());
   
   // Modify the fluid matrix.
   fptype t1 = 1 / dt + def->d_m;
   fptype st1 = SHIFT_AMOUNT * t1;
   fptype sigma_t1 = 1 / st1;
   SparseMatrixXfp JTMinv = J.transpose() * def->Minv;
   SparseMatrixXfp temp = JTMinv * J;
   for(int i = 0; i < temp.outerSize(); i++) {
      for(SparseMatrixXfp::InnerIterator it(temp, i); it; ++it) {
         Mts.push_back(Eigen::Triplet<fptype>(it.row(), it.col(),
            it.value() * sigma_t1));
      }
   }
   
   // Modify the off-diagonal blocks.
   fptype s2 = sigma_t1 * K_MULTIPLIER;
   fptype t2 = dt + def->d_s;
   SparseMatrixXfp C2 = t2 * def->B + (t1 - st1) * def->M;
   SparseMatrixXfp C2Minv = C2 * def->Minv;
   temp = C2Minv * J;
   for(int i = 0; i < temp.outerSize(); i++) {
      for(Eigen::SparseMatrix<fptype>::InnerIterator it(temp, i); it; ++it) {
         Mts.push_back(Eigen::Triplet<fptype>(np + it.row(), it.col(),
            it.value() * s2));
         Mts.push_back(Eigen::Triplet<fptype>(it.col(), np + it.row(),
            it.value() * s2));
      }
   }
   
   // Set the solid block.
   temp = sigma_t1 * C2Minv * C2 + C2;
   fptype ksq = K_MULTIPLIER * K_MULTIPLIER;
   for(int i = 0; i < temp.outerSize(); i++) {
      for(SparseMatrixXfp::InnerIterator it(temp, i); it; ++it) {
         Mts.push_back(Eigen::Triplet<fptype>(np + it.row(), np + it.col(),
            it.value() * ksq));
      }
   }
   
   // Modify the right hand side.
   VectorXfp MinvW2 = def->Minv * (def->B * def->xs2 - def->v) - def->vs2 / dt;
   rhs.head(np) += sigma_t1 * J.transpose() * MinvW2;
   rhs.tail(nv) = s2 * C2 * MinvW2;
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
   
   // Solve the linear system.
   M.setFromTriplets(Mts.begin(), Mts.end());
   if(info_stream.is_open()) {
      info_stream << M.nonZeros() << "\t";
   }
   Eigen::VectorXd p;
   timer = clock();
   if(iterative_solve) {
      Eigen::ConjugateGradient<SparseMatrixXfp, Eigen::Lower | Eigen::Upper,
         Eigen::IncompleteCholesky<fptype> > solver;
      solver.setTolerance(ITERATIVE_TOLERANCE);
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         exit(0);
      }
      
      Eigen::VectorXd p0(np + nv);
      p0.setZero();
      p0.tail(nv) = -def->vs2 / K_MULTIPLIER;
      p = solver.solveWithGuess(rhs, p0);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         std::cout << "Error: " << solver.error() << std::endl;
         exit(0);
      }
      
      if(info_stream.is_open()) {
         info_stream << solver.iterations() << "\t";
      }
   } else {
      Eigen::SimplicialLDLT<SparseMatrixXfp> solver;
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         exit(0);
      }
      
      p = solver.solve(rhs);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         exit(0);
      }
   }
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
   
   // Update the velocity field.
   timer = clock();
   for(int k = 1; k < ni; k++) {
      for(int j = 1; j < ni; j++) {
         for(int i = 1; i < ni; i++) {
            fptype p_n, p_p;
            
            // Update the y velocity component first.
            if(Fu(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(j == 1 || j == ni - 1) {
                  us(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j - 1, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) >= 0 && F(i, j - 1, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j - 1, k);
                  } else {
                     std::cout << "WTF [u]" << std::endl;
                  }
                  
                  us(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the x velocity component.
            if(Fv(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(i == 1 || i == ni - 1) {
                  vs(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i - 1, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) >= 0 && F(i - 1, j, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i - 1, j, k);
                  } else {
                     std::cout << "WTF [v]" << std::endl;
                  }
                  
                  vs(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the z velocity component.
            if(Fw(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(k == 1 || k == ni - 1) {
                  ws(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k - 1)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) >= 0 && F(i, j, k - 1) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j, k - 1);
                  } else {
                     std::cout << "WTF [w]" << std::endl;
                  }
                  
                  ws(i, j, k) -= alpha * (p_n - p_p);
               }
            }
         }
      }
   }
   
   // Copy the solid velocity data.
   def->vs2 = -K_MULTIPLIER * p.tail(nv);
   for(int i = 0; i < def->n_v; i++) {
      if(def->constrained[i] >= 0) {
         def->vs[3 * i] = def->vs2[3 * def->constrained[i]];
         def->vs[3 * i + 1] = def->vs2[3 * def->constrained[i] + 1];
         def->vs[3 * i + 2] = def->vs2[3 * def->constrained[i] + 2];
      }
   }
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
}

void Fluid::pProject_v2b(double dt) {
   clock_t timer;
   
   computePhi();
   setFAreas();
   int np = setF();
   timer = clock();
   def->intersectGrid(boundary, dx, F);
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t"
         << boundary.size() << "\t";
   }
   int nv = 3 * (def->n_v - def->n_constrained);
   
   std::vector<Eigen::Triplet<fptype> > Mts;
   SparseMatrixXfp M(np + nv, np + nv);
   VectorXfp rhs(np + nv);
   
   timer = clock();
   // Generate the linear system. This first loop takes care of the divergence-
   // free condition in regular cells.
   fptype alpha = dt / (rho * dx);
   for(int k = 1; k < ni - 1; k++) {
      for(int j = 1; j < ni - 1; j++) {
         for(int i = 1; i < ni - 1; i++) {
            int c_ind = F(i, j, k);
            
            if(c_ind < 0) {
               continue;
            }
            rhs[c_ind] = 0;
            
            // Take care of the left neighbour.
            int n_ind = F(i - 1, j, k);
            fptype term = Fv(i, j, k) * alpha;
            fptype rterm = Fv(i, j, k) * vs(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, -1, 0, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the right neighbour.
            n_ind = F(i + 1, j, k);
            term = Fv(i + 1, j, k) * alpha;
            rterm = Fv(i + 1, j, k) * vs(i + 1, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 1, 0, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the bottom neighbour.
            n_ind = F(i, j - 1, k);
            term = Fu(i, j, k) * alpha;
            rterm = Fu(i, j, k) * us(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, -1, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the top neighbour.
            n_ind = F(i, j + 1, k);
            term = Fu(i, j + 1, k) * alpha;
            rterm = Fu(i, j + 1, k) * us(i, j + 1, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 1, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the back neighbour.
            n_ind = F(i, j, k - 1);
            term = Fw(i, j, k) * alpha;
            rterm = Fw(i, j, k) * ws(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, -1)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the front neighbour.
            n_ind = F(i, j, k + 1);
            term = Fw(i, j, k + 1) * alpha;
            rterm = Fw(i, j, k + 1) * ws(i, j, k + 1);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, 1)));
               rhs[c_ind] -= rterm;
            }
         }
      }
   }
   
   // Computes the force Jacobian for the solid.
   std::vector<Eigen::Triplet<fptype> > Jts;
   SparseMatrixXfp J(nv, np);
   for(int i = 0; i < boundary.size(); i++) {
      BoundarySample& bs = boundary[i];
      
      for(int node = 0; node < 3; node++) {
         if(bs.n_index[node] >= 0) {
            int nindex = 3 * bs.n_index[node];
         
            for(int dim = 0; dim < 3; dim++) {
               Jts.push_back(Eigen::Triplet<fptype>(nindex + dim, bs.p_ind,
                  bs.barc[node] * bs.n[dim]));
            }
         }
      }
   }
   J.setFromTriplets(Jts.begin(), Jts.end());
   
   // Modify the fluid matrix.
   fptype t1 = 1 / dt + def->d_m;
   fptype st1 = SHIFT_AMOUNT * t1;
   fptype sigma_t1 = 1 / st1;
   SparseMatrixXfp JTMinv = J.transpose() * def->Minv;
   SparseMatrixXfp temp = JTMinv * J;
   for(int i = 0; i < temp.outerSize(); i++) {
      for(SparseMatrixXfp::InnerIterator it(temp, i); it; ++it) {
         Mts.push_back(Eigen::Triplet<fptype>(it.row(), it.col(),
            it.value() * sigma_t1));
      }
   }
   
   // Modify the right hand side.
   fptype t2 = dt + def->d_s;
   SparseMatrixXfp C2 = t2 * def->B + (t1 - st1) * def->M;
   VectorXfp MinvW2 = def->Minv * (def->B * def->xs2 - def->v) - def->vs2 / dt;
   rhs.head(np) += sigma_t1 * J.transpose() * MinvW2;
   rhs.tail(nv) = sigma_t1 * C2 * MinvW2;
   
   // Find an appropriate scaling factor for the velocities.
   fptype h_max = rhs.head(np).cwiseAbs().mean();
   fptype t_max = rhs.tail(nv).cwiseAbs().mean();
   fptype k_multiplier = -clamp(h_max / max(t_max, 1e-10), MIN_K_MULTIPLIER,
      MAX_K_MULTIPLIER);
   if(info_stream.is_open()) {
      info_stream << k_multiplier << "\t";
   }
   // Scale the right hand side.
   rhs.tail(nv) *= k_multiplier;
   
   // Modify the off-diagonal blocks.
   fptype s2 = sigma_t1 * k_multiplier;
   SparseMatrixXfp C2Minv = C2 * def->Minv;
   temp = C2Minv * J;
   for(int i = 0; i < temp.outerSize(); i++) {
      for(Eigen::SparseMatrix<fptype>::InnerIterator it(temp, i); it; ++it) {
         Mts.push_back(Eigen::Triplet<fptype>(np + it.row(), it.col(),
            it.value() * s2));
         Mts.push_back(Eigen::Triplet<fptype>(it.col(), np + it.row(),
            it.value() * s2));
      }
   }
   
   // Set the solid block.
   temp = sigma_t1 * C2Minv * C2 + C2;
   fptype ksq = k_multiplier * k_multiplier;
   for(int i = 0; i < temp.outerSize(); i++) {
      for(SparseMatrixXfp::InnerIterator it(temp, i); it; ++it) {
         Mts.push_back(Eigen::Triplet<fptype>(np + it.row(), np + it.col(),
            it.value() * ksq));
      }
   }
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
   
   // Solve the linear system.
   M.setFromTriplets(Mts.begin(), Mts.end());
   if(info_stream.is_open()) {
      info_stream << M.nonZeros() << "\t";
   }
   Eigen::VectorXd p;
   timer = clock();
   if(iterative_solve) {
      Eigen::ConjugateGradient<SparseMatrixXfp, Eigen::Lower | Eigen::Upper,
         Eigen::IncompleteCholesky<fptype> > solver;
      solver.setTolerance(ITERATIVE_TOLERANCE);
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         exit(0);
      }
      
      Eigen::VectorXd p0(np + nv);
      p0.setZero();
      p0.tail(nv) = -def->vs2 / k_multiplier;
      p = solver.solveWithGuess(rhs, p0);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         std::cout << "Error: " << solver.error() << std::endl;
         exit(0);
      }
      
      if(info_stream.is_open()) {
         info_stream << solver.iterations() << "\t";
      }
   } else {
      Eigen::SimplicialLDLT<SparseMatrixXfp> solver;
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         exit(0);
      }
      
      p = solver.solve(rhs);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         exit(0);
      }
   }
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
   
   // Update the velocity field.
   timer = clock();
   for(int k = 1; k < ni; k++) {
      for(int j = 1; j < ni; j++) {
         for(int i = 1; i < ni; i++) {
            fptype p_n, p_p;
            
            // Update the y velocity component first.
            if(Fu(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(j == 1 || j == ni - 1) {
                  us(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j - 1, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) >= 0 && F(i, j - 1, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j - 1, k);
                  } else {
                     std::cout << "WTF [u]" << std::endl;
                  }
                  
                  us(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the x velocity component.
            if(Fv(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(i == 1 || i == ni - 1) {
                  vs(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i - 1, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) >= 0 && F(i - 1, j, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i - 1, j, k);
                  } else {
                     std::cout << "WTF [v]" << std::endl;
                  }
                  
                  vs(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the z velocity component.
            if(Fw(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(k == 1 || k == ni - 1) {
                  ws(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k - 1)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) >= 0 && F(i, j, k - 1) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j, k - 1);
                  } else {
                     std::cout << "WTF [w]" << std::endl;
                  }
                  
                  ws(i, j, k) -= alpha * (p_n - p_p);
               }
            }
         }
      }
   }
   
   // Copy the solid velocity data.
   def->vs2 = -k_multiplier * p.tail(nv);
   for(int i = 0; i < def->n_v; i++) {
      if(def->constrained[i] >= 0) {
         def->vs[3 * i] = def->vs2[3 * def->constrained[i]];
         def->vs[3 * i + 1] = def->vs2[3 * def->constrained[i] + 1];
         def->vs[3 * i + 2] = def->vs2[3 * def->constrained[i] + 2];
      }
   }
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
}

void Fluid::pProject_v3(fptype dt) {
   computePhi();
   setFAreas();
   int np = setF();
   if(def != NULL) {
      def->intersectGrid(boundary, dx, F);
      if(solid_gravity) {
         def->addAcceleration(g, -dt);
      }
   }
   int nv = def == NULL ? 0 : 3 * (def->n_v - def->n_constrained);
   
   std::vector<Eigen::Triplet<fptype> > Mts;
   SparseMatrixXfp M(np, np);
   VectorXfp rhs(np);
   
   // Generate the linear system. This first loop takes care of the divergence-
   // free condition in regular cells.
   fptype alpha = dt / (rho * dx);
   for(int k = 1; k < ni - 1; k++) {
      for(int j = 1; j < ni - 1; j++) {
         for(int i = 1; i < ni - 1; i++) {
            int c_ind = F(i, j, k);
            
            if(c_ind < 0) {
               continue;
            }
            rhs[c_ind] = 0;
            
            // Take care of the left neighbour.
            int n_ind = F(i - 1, j, k);
            fptype term = Fv(i, j, k) * alpha;
            fptype rterm = Fv(i, j, k) * vs(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, -1, 0, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the right neighbour.
            n_ind = F(i + 1, j, k);
            term = Fv(i + 1, j, k) * alpha;
            rterm = Fv(i + 1, j, k) * vs(i + 1, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 1, 0, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the bottom neighbour.
            n_ind = F(i, j - 1, k);
            term = Fu(i, j, k) * alpha;
            rterm = Fu(i, j, k) * us(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, -1, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the top neighbour.
            n_ind = F(i, j + 1, k);
            term = Fu(i, j + 1, k) * alpha;
            rterm = Fu(i, j + 1, k) * us(i, j + 1, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 1, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the back neighbour.
            n_ind = F(i, j, k - 1);
            term = Fw(i, j, k) * alpha;
            rterm = Fw(i, j, k) * ws(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, -1)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the front neighbour.
            n_ind = F(i, j, k + 1);
            term = Fw(i, j, k + 1) * alpha;
            rterm = Fw(i, j, k + 1) * ws(i, j, k + 1);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, 1)));
               rhs[c_ind] -= rterm;
            }
         }
      }
   }
   
   // Computes the force Jacobian for the solid.
   std::vector<Eigen::Triplet<fptype> > Jts;
   SparseMatrixXfp J(nv, np);
   for(int i = 0; i < boundary.size(); i++) {
      BoundarySample& bs = boundary[i];
      
      for(int node = 0; node < 3; node++) {
         if(bs.n_index[node] >= 0) {
            int nindex = 3 * bs.n_index[node];
         
            for(int dim = 0; dim < 3; dim++) {
               Jts.push_back(Eigen::Triplet<fptype>(nindex + dim, bs.p_ind,
                  bs.barc[node] * bs.n[dim]));
            }
         }
      }
   }
   if(def != NULL) {
      J.setFromTriplets(Jts.begin(), Jts.end());
      rhs = rhs - J.transpose() * def->vs2;
   }
   
   // Solve the linear system.
   M.setFromTriplets(Mts.begin(), Mts.end());
   Eigen::VectorXd p;
   if(iterative_solve) {
      Eigen::ConjugateGradient<SparseMatrixXfp, Eigen::Lower | Eigen::Upper,
         Eigen::IncompleteCholesky<fptype> > solver;
      solver.setTolerance(ITERATIVE_TOLERANCE);
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         exit(0);
      }
      
      p = solver.solve(rhs);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         std::cout << "Error: " << solver.error() << std::endl;
         exit(0);
      }
   } else {
      Eigen::SimplicialLDLT<SparseMatrixXfp> solver;
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         exit(0);
      }
      
      p = solver.solve(rhs);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         exit(0);
      }
   }
   
   // Take care of solid damping and elasticity.
   if(def != NULL) {
      if(solid_gravity) {
         def->addAcceleration(g, dt);
      }
      
      SparseMatrixXfp solid = def->M / dt + dt * def->B - def->D;
      VectorXfp rhs2 = def->M * def->vs2 / dt - def->B * def->xs2 + def->v
         + J * p;
      
      if(iterative_solve) {
         Eigen::ConjugateGradient<SparseMatrixXfp, Eigen::Lower | Eigen::Upper,
            Eigen::IncompleteCholesky<fptype> > solver;
         solver.setTolerance(ITERATIVE_TOLERANCE);
         solver.compute(solid);
         if(solver.info() != Eigen::Success) {
            std::cout << "Eigen factorization failed.\n";
            exit(0);
         }
         
         def->vs2 = solver.solveWithGuess(rhs2, def->vs2);
         if(solver.info() != Eigen::Success) {
            std::cout << "Eigen solve failed.\n";
            std::cout << "Error: " << solver.error() << std::endl;
            exit(0);
         }
      } else {
         Eigen::SimplicialLDLT<SparseMatrixXfp> solver;
         solver.compute(solid);
         if(solver.info() != Eigen::Success) {
            std::cout << "Eigen factorization failed.\n";
            exit(0);
         }
         
         def->vs2 = solver.solve(rhs2);
         if(solver.info() != Eigen::Success) {
            std::cout << "Eigen solve failed.\n";
            exit(0);
         }
      }
   }
   
   // Update the velocity field.
   for(int k = 1; k < ni; k++) {
      for(int j = 1; j < ni; j++) {
         for(int i = 1; i < ni; i++) {
            fptype p_n, p_p;
            
            // Update the y velocity component first.
            if(Fu(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(j == 1 || j == ni - 1) {
                  us(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j - 1, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) >= 0 && F(i, j - 1, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j - 1, k);
                  } else {
                     std::cout << "WTF [u]" << std::endl;
                  }
                  
                  us(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the x velocity component.
            if(Fv(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(i == 1 || i == ni - 1) {
                  vs(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i - 1, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) >= 0 && F(i - 1, j, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i - 1, j, k);
                  } else {
                     std::cout << "WTF [v]" << std::endl;
                  }
                  
                  vs(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the z velocity component.
            if(Fw(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(k == 1 || k == ni - 1) {
                  ws(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k - 1)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) >= 0 && F(i, j, k - 1) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j, k - 1);
                  } else {
                     std::cout << "WTF [w]" << std::endl;
                  }
                  
                  ws(i, j, k) -= alpha * (p_n - p_p);
               }
            }
         }
      }
   }
   
   // Copy the solid velocity data.
   if(def != NULL) {
      for(int i = 0; i < def->n_v; i++) {
         if(def->constrained[i] >= 0) {
            def->vs[3 * i] = def->vs2[3 * def->constrained[i]];
            def->vs[3 * i + 1] = def->vs2[3 * def->constrained[i] + 1];
            def->vs[3 * i + 2] = def->vs2[3 * def->constrained[i] + 2];
         }
      }
   }
}

void Fluid::pProject_v4(fptype dt) {
   clock_t timer;
   
   computePhi();
   setFAreas();
   int np = setF();
   if(def != NULL) {
      timer = clock();
      def->intersectGrid(boundary, dx, F);
      if(info_stream.is_open()) {
         info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t"
            << boundary.size() << "\t";
      }
   }
   int nv = def == NULL ? 0 : 3 * (def->n_v - def->n_constrained);
   
   std::vector<Eigen::Triplet<fptype> > Mts;
   SparseMatrixXfp M(np + nv, np + nv);
   VectorXfp rhs(np + nv);
   
   timer = clock();
   // Generate the linear system. This first loop takes care of the divergence-
   // free condition in regular cells.
   fptype alpha = dt / (rho * dx);
   for(int k = 1; k < ni - 1; k++) {
      for(int j = 1; j < ni - 1; j++) {
         for(int i = 1; i < ni - 1; i++) {
            int c_ind = F(i, j, k);
            
            if(c_ind < 0) {
               continue;
            }
            rhs[c_ind] = 0;
            
            // Take care of the left neighbour.
            int n_ind = F(i - 1, j, k);
            fptype term = Fv(i, j, k) * alpha;
            fptype rterm = Fv(i, j, k) * vs(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, -1, 0, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the right neighbour.
            n_ind = F(i + 1, j, k);
            term = Fv(i + 1, j, k) * alpha;
            rterm = Fv(i + 1, j, k) * vs(i + 1, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 1, 0, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the bottom neighbour.
            n_ind = F(i, j - 1, k);
            term = Fu(i, j, k) * alpha;
            rterm = Fu(i, j, k) * us(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, -1, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the top neighbour.
            n_ind = F(i, j + 1, k);
            term = Fu(i, j + 1, k) * alpha;
            rterm = Fu(i, j + 1, k) * us(i, j + 1, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 1, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the back neighbour.
            n_ind = F(i, j, k - 1);
            term = Fw(i, j, k) * alpha;
            rterm = Fw(i, j, k) * ws(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, -1)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the front neighbour.
            n_ind = F(i, j, k + 1);
            term = Fw(i, j, k + 1) * alpha;
            rterm = Fw(i, j, k + 1) * ws(i, j, k + 1);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, 1)));
               rhs[c_ind] -= rterm;
            }
         }
      }
   }
   
   // Computes the force Jacobian for the solid.
   for(int i = 0; i < boundary.size(); i++) {
      BoundarySample& bs = boundary[i];
      
      for(int node = 0; node < 3; node++) {
         if(bs.n_index[node] >= 0) {
            int nindex = np + 3 * bs.n_index[node];
         
            for(int dim = 0; dim < 3; dim++) {
               Mts.push_back(Eigen::Triplet<fptype>(nindex + dim, bs.p_ind,
                  bs.barc[node] * bs.n[dim]));
               Mts.push_back(Eigen::Triplet<fptype>(bs.p_ind, nindex + dim,
                  bs.barc[node] * bs.n[dim]));
            }
         }
      }
   }
   
   // Take care of solid damping and elasticity.
   if(def != NULL) {
      def->stepE(dt);
      
      SparseMatrixXfp solid = def->M / dt - def->D;
      for(int i = 0; i < solid.outerSize(); i++) {
         for(SparseMatrixXfp::InnerIterator it(solid, i); it; ++it) {
            Mts.push_back(Eigen::Triplet<fptype>(np + it.row(), np + it.col(),
               -it.value()));
         }
      }
      
      rhs.tail(nv) = -def->M * def->vs2 / dt;
   }
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
   
   // Solve the linear system.
   M.setFromTriplets(Mts.begin(), Mts.end());
   if(info_stream.is_open()) {
      info_stream << M.nonZeros() << "\t";
   }
   Eigen::VectorXd p;
   timer = clock();
   if(iterative_solve) {
      Eigen::BiCGSTAB<SparseMatrixXfp, Eigen::IncompleteLUT<fptype> > solver;
      solver.setTolerance(ITERATIVE_TOLERANCE);
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         exit(0);
      }
      
      Eigen::VectorXd p0(np + nv);
      p0.setZero();
      if(def != NULL) {
         p0.tail(nv) = def->vs2;
      }
      p = solver.solveWithGuess(rhs, p0);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         std::cout << "Error: " << solver.error() << std::endl;
         exit(0);
      }
      
      if(info_stream.is_open()) {
         info_stream << solver.iterations() << "\t";
      }
   } else {
      Eigen::SparseLU<Eigen::SparseMatrix<fptype> > solver;
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         exit(0);
      }
      
      p = solver.solve(rhs);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         exit(0);
      }
   }
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
   
   // Update the velocity field.
   timer = clock();
   for(int k = 1; k < ni; k++) {
      for(int j = 1; j < ni; j++) {
         for(int i = 1; i < ni; i++) {
            fptype p_n, p_p;
            
            // Update the y velocity component first.
            if(Fu(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(j == 1 || j == ni - 1) {
                  us(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j - 1, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) >= 0 && F(i, j - 1, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j - 1, k);
                  } else {
                     std::cout << "WTF [u]" << std::endl;
                  }
                  
                  us(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the x velocity component.
            if(Fv(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(i == 1 || i == ni - 1) {
                  vs(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i - 1, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) >= 0 && F(i - 1, j, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i - 1, j, k);
                  } else {
                     std::cout << "WTF [v]" << std::endl;
                  }
                  
                  vs(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the z velocity component.
            if(Fw(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(k == 1 || k == ni - 1) {
                  ws(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k - 1)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) >= 0 && F(i, j, k - 1) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j, k - 1);
                  } else {
                     std::cout << "WTF [w]" << std::endl;
                  }
                  
                  ws(i, j, k) -= alpha * (p_n - p_p);
               }
            }
         }
      }
   }
   
   // Copy the solid velocity data.
   if(def != NULL) {
      def->vs2 = p.tail(nv);
      
      for(int i = 0; i < def->n_v; i++) {
         if(def->constrained[i] >= 0) {
            def->vs[3 * i] = def->vs2[3 * def->constrained[i]];
            def->vs[3 * i + 1] = def->vs2[3 * def->constrained[i] + 1];
            def->vs[3 * i + 2] = def->vs2[3 * def->constrained[i] + 2];
         }
      }
   }
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
}

void Fluid::pProject_v5(double dt) {
   clock_t timer;
   
   computePhi();
   setFAreas();
   int np = setF();
   timer = clock();
   def->intersectGrid(boundary, dx, F, MIN_FACE_AREA * dx * dx / 2);
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t"
         << boundary.size() << "\t";
   }
   int nv = 3 * (def->n_v - def->n_constrained);
   
   std::vector<Eigen::Triplet<fptype> > Mts;
   SparseMatrixXfp M(np + nv, np + nv);
   VectorXfp rhs(np + nv);
   
   timer = clock();
   // Generate the linear system. This first loop takes care of the divergence-
   // free condition in regular cells.
   fptype alpha = dt / (rho * dx);
   for(int k = 1; k < ni - 1; k++) {
      for(int j = 1; j < ni - 1; j++) {
         for(int i = 1; i < ni - 1; i++) {
            int c_ind = F(i, j, k);
            
            if(c_ind < 0) {
               continue;
            }
            rhs[c_ind] = 0;
            
            // Take care of the left neighbour.
            int n_ind = F(i - 1, j, k);
            fptype term = Fv(i, j, k) * alpha;
            fptype rterm = Fv(i, j, k) * vs(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, -1, 0, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the right neighbour.
            n_ind = F(i + 1, j, k);
            term = Fv(i + 1, j, k) * alpha;
            rterm = Fv(i + 1, j, k) * vs(i + 1, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 1, 0, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the bottom neighbour.
            n_ind = F(i, j - 1, k);
            term = Fu(i, j, k) * alpha;
            rterm = Fu(i, j, k) * us(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, -1, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the top neighbour.
            n_ind = F(i, j + 1, k);
            term = Fu(i, j + 1, k) * alpha;
            rterm = Fu(i, j + 1, k) * us(i, j + 1, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 1, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the back neighbour.
            n_ind = F(i, j, k - 1);
            term = Fw(i, j, k) * alpha;
            rterm = Fw(i, j, k) * ws(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, -1)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the front neighbour.
            n_ind = F(i, j, k + 1);
            term = Fw(i, j, k + 1) * alpha;
            rterm = Fw(i, j, k + 1) * ws(i, j, k + 1);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, 1)));
               rhs[c_ind] -= rterm;
            }
         }
      }
   }
   
   // Computes the force Jacobian for the solid.
   std::vector<Eigen::Triplet<fptype> > Jts;
   SparseMatrixXfp J(nv, np);
   for(int i = 0; i < boundary.size(); i++) {
      BoundarySample& bs = boundary[i];
      
      for(int node = 0; node < 3; node++) {
         if(bs.n_index[node] >= 0) {
            int nindex = 3 * bs.n_index[node];
         
            for(int dim = 0; dim < 3; dim++) {
               Jts.push_back(Eigen::Triplet<fptype>(nindex + dim, bs.p_ind,
                  bs.barc[node] * bs.n[dim]));
            }
         }
      }
   }
   J.setFromTriplets(Jts.begin(), Jts.end());
   
   // Factor solid stiffness.
   Eigen::SimplicialLLT<SparseMatrixXfp, Eigen::Lower,
      Eigen::NaturalOrdering<int> > cholesky;
   cholesky.compute((dt + def->d_s) * def->B);
   if(cholesky.info() != Eigen::Success) {
      std::cout << "Eigen Cholesky factorization failed.\n";
      exit(0);
   }
   Eigen::SimplicialLLT<SparseMatrixXfp, Eigen::Lower,
      Eigen::NaturalOrdering<int> >::Traits::MatrixU C = cholesky.matrixU();
   
   // Get the scaled mass inverse matrix.
   SparseMatrixXfp Minv = (1 / (1 / dt + def->d_m)) * def->Minv;
   
   // Modify the fluid matrix.
   SparseMatrixXfp JTMinv = J.transpose() * Minv;
   SparseMatrixXfp temp = JTMinv * J;
   for(int i = 0; i < temp.outerSize(); i++) {
      for(SparseMatrixXfp::InnerIterator it(temp, i); it; ++it) {
         Mts.push_back(Eigen::Triplet<fptype>(it.row(), it.col(),
            it.value()));
      }
   }
   
   // Modify the off-diagonal blocks.
   SparseMatrixXfp CMinv = -C * Minv;
   temp = CMinv * J;
   for(int i = 0; i < temp.outerSize(); i++) {
      for(Eigen::SparseMatrix<fptype>::InnerIterator it(temp, i); it; ++it) {
         Mts.push_back(Eigen::Triplet<fptype>(np + it.row(), it.col(),
            it.value() * K_MULTIPLIER));
         Mts.push_back(Eigen::Triplet<fptype>(it.col(), np + it.row(),
            it.value() * K_MULTIPLIER));
      }
   }
   
   // Set the solid block.
   temp = -CMinv * cholesky.matrixL();
   for(int i = 0; i < temp.outerSize(); i++) {
      for(SparseMatrixXfp::InnerIterator it(temp, i); it; ++it) {
         Mts.push_back(Eigen::Triplet<fptype>(np + it.row(), np + it.col(),
            it.value() * K_MULTIPLIER * K_MULTIPLIER));
      }
   }
   for(int i = 0; i < nv; i++) {
      Mts.push_back(Eigen::Triplet<fptype>(np + i, np + i,
         K_MULTIPLIER * K_MULTIPLIER));
   }
   
   // Modify the right hand side.
   VectorXfp w = -def->M * def->vs2 / dt + def->B * def->xs2 - def->v;
   rhs.head(np) += JTMinv * w;
   rhs.tail(nv) = K_MULTIPLIER * CMinv * w;
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
   
   // Solve the linear system.
   M.setFromTriplets(Mts.begin(), Mts.end());
   if(info_stream.is_open()) {
      info_stream << M.nonZeros() << "\t";
   }
   Eigen::VectorXd p;
   timer = clock();
   if(iterative_solve) {
      Eigen::ConjugateGradient<SparseMatrixXfp, Eigen::Lower | Eigen::Upper,
         Eigen::IncompleteCholesky<fptype> > solver;
      solver.setTolerance(ITERATIVE_TOLERANCE);
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         exit(0);
      }
      
      Eigen::VectorXd p0(np + nv);
      p0.setZero();
      p0.tail(nv) = C * def->vs2 / K_MULTIPLIER;
      p = solver.solveWithGuess(rhs, p0);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         std::cout << "Error: " << solver.error() << std::endl;
         exit(0);
      }
      
      if(info_stream.is_open()) {
         info_stream << solver.iterations() << "\t";
      }
   } else {
      Eigen::SimplicialLDLT<SparseMatrixXfp> solver;
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         exit(0);
      }
      
      p = solver.solve(rhs);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         exit(0);
      }
   }
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
   
   // Update the velocity field.
   timer = clock();
   for(int k = 1; k < ni; k++) {
      for(int j = 1; j < ni; j++) {
         for(int i = 1; i < ni; i++) {
            fptype p_n, p_p;
            
            // Update the y velocity component first.
            if(Fu(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(j == 1 || j == ni - 1) {
                  us(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j - 1, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) >= 0 && F(i, j - 1, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j - 1, k);
                  } else {
                     std::cout << "WTF [u]" << std::endl;
                  }
                  
                  us(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the x velocity component.
            if(Fv(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(i == 1 || i == ni - 1) {
                  vs(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i - 1, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) >= 0 && F(i - 1, j, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i - 1, j, k);
                  } else {
                     std::cout << "WTF [v]" << std::endl;
                  }
                  
                  vs(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the z velocity component.
            if(Fw(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(k == 1 || k == ni - 1) {
                  ws(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k - 1)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) >= 0 && F(i, j, k - 1) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j, k - 1);
                  } else {
                     std::cout << "WTF [w]" << std::endl;
                  }
                  
                  ws(i, j, k) -= alpha * (p_n - p_p);
               }
            }
         }
      }
   }
   
   // Copy the solid velocity data.
   def->vs2 = C.solve(p.tail(nv) * K_MULTIPLIER);
   for(int i = 0; i < def->n_v; i++) {
      if(def->constrained[i] >= 0) {
         def->vs[3 * i] = def->vs2[3 * def->constrained[i]];
         def->vs[3 * i + 1] = def->vs2[3 * def->constrained[i] + 1];
         def->vs[3 * i + 2] = def->vs2[3 * def->constrained[i] + 2];
      }
   }
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
}

void Fluid::pProject_v6(double dt) {
   clock_t timer;
   
   computePhi();
   setFAreas();
   int np = setF();
   timer = clock();
   def->intersectGrid(boundary, dx, F, MIN_FACE_AREA * dx * dx / 2);
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t"
         << boundary.size() << "\t";
   }
   int nv = 3 * (def->n_v - def->n_constrained);
   
   std::vector<Eigen::Triplet<fptype> > Mts;
   SparseMatrixXfp M(np + 6 * def->n_t, np + 6 * def->n_t);
   VectorXfp rhs(np + 6 * def->n_t);
   
   timer = clock();
   // Generate the linear system. This first loop takes care of the divergence-
   // free condition in regular cells.
   fptype alpha = dt / (rho * dx);
   for(int k = 1; k < ni - 1; k++) {
      for(int j = 1; j < ni - 1; j++) {
         for(int i = 1; i < ni - 1; i++) {
            int c_ind = F(i, j, k);
            
            if(c_ind < 0) {
               continue;
            }
            rhs[c_ind] = 0;
            
            // Take care of the left neighbour.
            int n_ind = F(i - 1, j, k);
            fptype term = Fv(i, j, k) * alpha;
            fptype rterm = Fv(i, j, k) * vs(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, -1, 0, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the right neighbour.
            n_ind = F(i + 1, j, k);
            term = Fv(i + 1, j, k) * alpha;
            rterm = Fv(i + 1, j, k) * vs(i + 1, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 1, 0, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the bottom neighbour.
            n_ind = F(i, j - 1, k);
            term = Fu(i, j, k) * alpha;
            rterm = Fu(i, j, k) * us(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, -1, 0)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the top neighbour.
            n_ind = F(i, j + 1, k);
            term = Fu(i, j + 1, k) * alpha;
            rterm = Fu(i, j + 1, k) * us(i, j + 1, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 1, 0)));
               rhs[c_ind] -= rterm;
            }
            
            // Take care of the back neighbour.
            n_ind = F(i, j, k - 1);
            term = Fw(i, j, k) * alpha;
            rterm = Fw(i, j, k) * ws(i, j, k);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] += rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, -1)));
               rhs[c_ind] += rterm;
            }
            
            // Take care of the front neighbour.
            n_ind = F(i, j, k + 1);
            term = Fw(i, j, k + 1) * alpha;
            rterm = Fw(i, j, k + 1) * ws(i, j, k + 1);
            if(n_ind >= 0) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, n_ind, -term));
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind, term));
               rhs[c_ind] -= rterm;
            } else if(n_ind == -1) {
               Mts.push_back(Eigen::Triplet<fptype>(c_ind, c_ind,
                  term / getTheta(i, j, k, 0, 0, 1)));
               rhs[c_ind] -= rterm;
            }
         }
      }
   }
   
   // Computes the force Jacobian for the solid.
   std::vector<Eigen::Triplet<fptype> > Jts;
   SparseMatrixXfp J(nv, np);
   for(int i = 0; i < boundary.size(); i++) {
      BoundarySample& bs = boundary[i];
      
      for(int node = 0; node < 3; node++) {
         if(bs.n_index[node] >= 0) {
            int nindex = 3 * bs.n_index[node];
         
            for(int dim = 0; dim < 3; dim++) {
               Jts.push_back(Eigen::Triplet<fptype>(nindex + dim, bs.p_ind,
                  bs.barc[node] * bs.n[dim]));
            }
         }
      }
   }
   J.setFromTriplets(Jts.begin(), Jts.end());
   
   // Factor solid stiffness.
   SparseMatrixXfp C = sqrt(dt + def->d_s) * def->C;
   // Get the scaled mass inverse matrix.
   SparseMatrixXfp Minv = (1 / (1 / dt + def->d_m)) * def->Minv;
   
   // Modify the fluid matrix.
   SparseMatrixXfp JTMinv = J.transpose() * Minv;
   SparseMatrixXfp temp = JTMinv * J;
   for(int i = 0; i < temp.outerSize(); i++) {
      for(SparseMatrixXfp::InnerIterator it(temp, i); it; ++it) {
         Mts.push_back(Eigen::Triplet<fptype>(it.row(), it.col(),
            it.value()));
      }
   }
   
   // Modify the off-diagonal blocks.
   SparseMatrixXfp CMinv = -C * Minv;
   temp = CMinv * J;
   for(int i = 0; i < temp.outerSize(); i++) {
      for(Eigen::SparseMatrix<fptype>::InnerIterator it(temp, i); it; ++it) {
         Mts.push_back(Eigen::Triplet<fptype>(np + it.row(), it.col(),
            it.value() * K_MULTIPLIER));
         Mts.push_back(Eigen::Triplet<fptype>(it.col(), np + it.row(),
            it.value() * K_MULTIPLIER));
      }
   }
   
   // Set the solid block.
   temp = -CMinv * C.transpose();
   for(int i = 0; i < temp.outerSize(); i++) {
      for(SparseMatrixXfp::InnerIterator it(temp, i); it; ++it) {
         Mts.push_back(Eigen::Triplet<fptype>(np + it.row(), np + it.col(),
            it.value() * K_MULTIPLIER * K_MULTIPLIER));
      }
   }
   for(int i = 0; i < 6 * def->n_t; i++) {
      Mts.push_back(Eigen::Triplet<fptype>(np + i, np + i,
         K_MULTIPLIER * K_MULTIPLIER));
   }
   
   // Modify the right hand side.
   VectorXfp w = -def->M * def->vs2 / dt
      + def->C.transpose() * def->C * def->xs2 - def->v;
   rhs.head(np) += JTMinv * w;
   rhs.tail(6 * def->n_t) = K_MULTIPLIER * CMinv * w;
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
   
   // Solve the linear system.
   M.setFromTriplets(Mts.begin(), Mts.end());
   if(info_stream.is_open()) {
      info_stream << M.nonZeros() << "\t";
   }
   Eigen::VectorXd p;
   timer = clock();
   if(iterative_solve) {
      Eigen::ConjugateGradient<SparseMatrixXfp, Eigen::Lower | Eigen::Upper,
         Eigen::IncompleteCholesky<fptype> > solver;
      solver.setTolerance(ITERATIVE_TOLERANCE);
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         Eigen::saveMarket(def->M, "M.mat");
         exit(0);
      }
      
      Eigen::VectorXd p0(np + 6 * def->n_t);
      p0.setZero();
      p0.tail(6 * def->n_t) = C * def->vs2 / K_MULTIPLIER;
      p = solver.solveWithGuess(rhs, p0);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         std::cout << "Error: " << solver.error() << std::endl;
         exit(0);
      }
      
      if(info_stream.is_open()) {
         info_stream << solver.iterations() << "\t";
      }
   } else {
      Eigen::SimplicialLDLT<SparseMatrixXfp> solver;
      solver.compute(M);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen factorization failed.\n";
         exit(0);
      }
      
      p = solver.solve(rhs);
      if(solver.info() != Eigen::Success) {
         std::cout << "Eigen solve failed.\n";
         exit(0);
      }
   }
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
   
   // Update the velocity field.
   timer = clock();
   for(int k = 1; k < ni; k++) {
      for(int j = 1; j < ni; j++) {
         for(int i = 1; i < ni; i++) {
            fptype p_n, p_p;
            
            // Update the y velocity component first.
            if(Fu(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(j == 1 || j == ni - 1) {
                  us(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) >= 0) {
                     p_n = p[F(i, j - 1, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                     p_p = p[F(i, j - 1, k)];
                  } else if(F(i, j, k) >= 0 && F(i, j - 1, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, -1, 0));
                  } else if(F(i, j, k) == -1 && F(i, j - 1, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j - 1, k);
                  } else {
                     std::cout << "WTF [u]" << std::endl;
                  }
                  
                  us(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the x velocity component.
            if(Fv(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(i == 1 || i == ni - 1) {
                  vs(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) >= 0) {
                     p_n = p[F(i - 1, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                     p_p = p[F(i - 1, j, k)];
                  } else if(F(i, j, k) >= 0 && F(i - 1, j, k) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, -1, 0, 0));
                  } else if(F(i, j, k) == -1 && F(i - 1, j, k) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i - 1, j, k);
                  } else {
                     std::cout << "WTF [v]" << std::endl;
                  }
                  
                  vs(i, j, k) -= alpha * (p_n - p_p);
               }
            }
            
            // Update the z velocity component.
            if(Fw(i, j, k) > 0) {
               // Set the velocity to 0 if the cell is in contact with the
               // container.
               if(k == 1 || k == ni - 1) {
                  ws(i, j, k) = 0;
               } else {
                  if(F(i, j, k) >= 0 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) >= 0) {
                     p_n = p[F(i, j, k - 1)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                     p_p = p[F(i, j, k - 1)];
                  } else if(F(i, j, k) >= 0 && F(i, j, k - 1) == -1) {
                     p_n = p[F(i, j, k)];
                     p_p = p[F(i, j, k)] * (1 - 1
                        / getTheta(i, j, k, 0, 0, -1));
                  } else if(F(i, j, k) == -1 && F(i, j, k - 1) == -1) {
                     p_n = getOPressure(p, i, j, k);
                     p_p = getOPressure(p, i, j, k - 1);
                  } else {
                     std::cout << "WTF [w]" << std::endl;
                  }
                  
                  ws(i, j, k) -= alpha * (p_n - p_p);
               }
            }
         }
      }
   }
   
   // Get the solid velocity data.
   def->vs2 = Minv * (J * p.head(np)
      - K_MULTIPLIER * C.transpose() * p.tail(6 * def->n_t) - w);
   // Copy the solid velocity data.
   for(int i = 0; i < def->n_v; i++) {
      if(def->constrained[i] >= 0) {
         def->vs[3 * i] = def->vs2[3 * def->constrained[i]];
         def->vs[3 * i + 1] = def->vs2[3 * def->constrained[i] + 1];
         def->vs[3 * i + 2] = def->vs2[3 * def->constrained[i] + 2];
      }
   }
   
   if(info_stream.is_open()) {
      info_stream << (double) (clock() - timer) / CLOCKS_PER_SEC << "\t";
   }
}

fptype Fluid::getOPressure(const VectorXfp& p, int i, int j, int k) {
   if(F(i, j, k) >= 0) {
      return p[F(i, j, k)];
   }
   
   // Look at the fluid neighbours.
   int count = 0;
   fptype sum = 0;
   // Left neighbour...
   if(F(i - 1, j, k) >= 0) {
      sum += (1 - 1 / getTheta(i, j, k, -1, 0, 0)) * p[F(i - 1, j, k)];
      count++;
   }
   // Right neighbour...
   if(F(i + 1, j, k) >= 0) {
      sum += (1 - 1 / getTheta(i, j, k, 1, 0, 0)) * p[F(i + 1, j, k)];
      count++;
   }
   // Bottom neighbour...
   if(F(i, j - 1, k) >= 0) {
      sum += (1 - 1 / getTheta(i, j, k, 0, -1, 0)) * p[F(i, j - 1, k)];
      count++;
   }
   // Top neighbour...
   if(F(i, j + 1, k) >= 0) {
      sum += (1 - 1 / getTheta(i, j, k, 0, 1, 0)) * p[F(i, j + 1, k)];
      count++;
   }
   // Back neighbour...
   if(F(i, j, k - 1) >= 0) {
      sum += (1 - 1 / getTheta(i, j, k, 0, 0, -1)) * p[F(i, j, k - 1)];
      count++;
   }
   // Front neighbour...
   if(F(i, j, k + 1) >= 0) {
      sum += (1 - 1 / getTheta(i, j, k, 0, 0, 1)) * p[F(i, j, k + 1)];
      count++;
   }
   
   if(count > 0) {
      return sum / count;
   }
   
   return 0;
}

void Fluid::setValid() {
   for(int k = 1; k < ni; k++) {
      for(int j = 1; j < ni; j++) {
         for(int i = 1; i < ni; i++) {
            Fu(i, j, k) = (Fu(i, j, k) > 0 && (F(i, j, k) >= 0
               || F(i, j - 1, k) >= 0)) ? 1 : 0;
            Fv(i, j, k) = (Fv(i, j, k) > 0 && (F(i, j, k) >= 0
               || F(i - 1, j, k) >= 0)) ? 1 : 0;
            Fw(i, j, k) = (Fw(i, j, k) > 0 && (F(i, j, k) >= 0
               || F(i, j, k - 1) >= 0)) ? 1 : 0;
         }
      }
   }
}

void Fluid::constrainVelocities() {
   temp_us = us;
   temp_vs = vs;
   temp_ws = ws;
   
   def->projectNormalV(dx, temp_us, Fu, solid_phi, *this, 1);
   def->projectNormalV(dx, temp_vs, Fv, solid_phi, *this, 0);
   def->projectNormalV(dx, temp_ws, Fw, solid_phi, *this, 2);
   
   us = temp_us;
   vs = temp_vs;
   ws = temp_ws;
}
