#include <cstdio>
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include <GL/glu.h>

#include "gluvi.h"
#include "openglutils.h"

// Simple viewer for liquid simulator data
// Hold shift and use the mouse buttons to manipulate the camera

using namespace std;

string frame_number="frame 0";

string file_path;

int frame = 0;
int highest_frame = 0;

bool filming = false;
bool running = false;

char *ppmfileformat;

std::vector<Vec3f> particles;
float particle_radius;

std::vector<Vec3f> vertices;
std::vector<Vec3f> normals;
std::vector<Vec3ui> tris;
int nodes;

bool non_inter;

bool draw_fluid = true;
bool draw_solid = true;

bool get_tris() {
   std::ostringstream strout;
   
   strout << file_path << "surface.txt";
   std::ifstream surface(strout.str().c_str());
   if(!surface.good()) {
      printf("Failed to open surface file.\n");
      return false;
   }
   
   int nb;
   surface >> nodes >> nb;
   for(int p = 0; p < nb; p++) {
      Vec3ui tri;
      surface >> tri[0] >> tri[1] >> tri[2];
      tris.push_back(tri);
   }
   
   vertices.resize(nodes);
   normals.resize(nodes);
   return true;
}

bool read_frame(int newframe)
{
   if(newframe < 0) {
      newframe = highest_frame;
   }
   
   std::ostringstream strout;
   
   strout << file_path << "frame_" << newframe << ".txt";
   std::ifstream particles_in(strout.str().c_str());
   if(!particles_in.good()) {
      if(non_inter) {
         exit(0);
      }
      printf("Failed to open frame file.\n");
      return false;
   }
   
   unsigned int p_count;
   particles_in >> p_count >> particle_radius;
   particles.resize(p_count);
   for(unsigned int p = 0; p < p_count; ++p) {
      Vec3f pos;
      particles_in >> pos[0] >> pos[1] >> pos[2];
      particles[p] = pos;
   }
   
   for(int i = 0; i < nodes; i++) {
      Vec3f node;
      particles_in >> node[0] >> node[1] >> node[2];
      vertices[i] = node;
      
      normals[i][0] = 0;
      normals[i][1] = 0;
      normals[i][2] = 0;
   }
   for(int i = 0; i < tris.size(); i++) {
      Vec3f normal = cross(vertices[tris[i][1]] - vertices[tris[i][0]],
         vertices[tris[i][2]] - vertices[tris[i][0]]);
      normals[tris[i][0]] += normal;
      normals[tris[i][1]] += normal;
      normals[tris[i][2]] += normal;
   }
   for(int i = 0; i < nodes; i++) {
      normalize(normals[i]);
   }

   if(newframe > highest_frame) {
      highest_frame = newframe;
   }

   strout.str("");

   frame = newframe;

   strout.str("");
   strout << "frame " << frame;
   frame_number = strout.str();

   return true;
}

void set_view(Gluvi::Target3D &cam)
{
   cam.dist = 1.75;//2;//2.25;
   cam.target[0] = 0.5;
   cam.target[1] = 0.45;
   cam.target[2] = 0.5;
   cam.pitch = -0.125;//-0.25;//-0.8;
   cam.heading = 0;//M_PI / 2;//0;//M_PI / 4;
}

void set_lights_and_material(int object)
{
   glEnable(GL_LIGHTING);
   GLfloat global_ambient[4] = {0.1f, 0.1f, 0.1f, 1.0f};
   glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);
   glShadeModel(GL_SMOOTH);

   //Light #1
   GLfloat color[4] = {1.0f, 1.0f, 1.0f, 1.0f};
   GLfloat position[3] = {1.5f, 1.0f, 0.5f};
   glLightfv(GL_LIGHT0, GL_SPECULAR, color);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, color);
   glLightfv(GL_LIGHT0, GL_POSITION, position);

   //Light #2
   GLfloat color2[4] = {1.0f, 1.0f, 1.0f, 1.0f};
   GLfloat position2[3] = {1.5f, 1.0f, 0.5f};
   glLightfv(GL_LIGHT1, GL_SPECULAR, color2);
   glLightfv(GL_LIGHT1, GL_DIFFUSE, color2);
   glLightfv(GL_LIGHT1, GL_POSITION, position2);

   if(object == 1) {
      GLfloat obj_color[4] = {.2, .3, .7};
      glMaterialfv (GL_FRONT, GL_AMBIENT, obj_color);
      glMaterialfv (GL_FRONT, GL_DIFFUSE, obj_color);

      GLfloat specular[4] = {.4, .2, .8};
      glMaterialf (GL_FRONT, GL_SHININESS, 32);
      glMaterialfv (GL_FRONT, GL_SPECULAR, specular);
   } else if(object == 2) {
      GLfloat obj_color[4] = {.8, .8, .1};
      glMaterialfv (GL_FRONT, GL_AMBIENT, obj_color);
      glMaterialfv (GL_FRONT, GL_DIFFUSE, obj_color);

      GLfloat specular[4] = {.4, .2, .8};
      glMaterialf (GL_FRONT, GL_SHININESS, 16);
      glMaterialfv (GL_FRONT, GL_SPECULAR, specular);
   }
   
   glEnable(GL_LIGHT0);
   glEnable(GL_LIGHT1);
}

void timer(int value)
{
   if(filming) {
      Gluvi::ppm_screenshot(ppmfileformat, frame);
      if(read_frame(frame + 1)) {
         if(frame == 0) {
            filming = false;
         }
      }
      glutPostRedisplay();
      glutTimerFunc(1.0 / 60, timer, 0);
   }


   if(running) {
      read_frame(frame + 1);
      glutTimerFunc(1.0 / 60, timer, 0);
      glutPostRedisplay();
   }
}


void display(void)
{
   glClearColor(0.6f, 0.7f, 0.9f, 1);

   //Coordinate system
   glDisable(GL_LIGHTING);
   glBegin(GL_LINES);
   glColor3f(1,0,0); glVertex3f(0,0,0); glVertex3f(0.1,0,0);
   glColor3f(0,1,0); glVertex3f(0,0,0); glVertex3f(0,0.1,0);
   glColor3f(0,0,1); glVertex3f(0,0,0); glVertex3f(0,0,0.1);
   glEnd();

   if(draw_fluid) {
      glEnable(GL_LIGHTING);
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

      set_lights_and_material(1); 

      //Draw the liquid particles as simple spheres for now.
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINES);
      GLUquadric* particle_sphere;
      particle_sphere = gluNewQuadric();
      gluQuadricDrawStyle(particle_sphere, GLU_FILL );
      for(unsigned int p = 0; p < particles.size(); ++p) {
         glPushMatrix();
         Vec3f pos = particles[p].v;
         glTranslatef(pos[0], pos[1], pos[2]);
         gluSphere(particle_sphere, particle_radius, 20, 20);
         glPopMatrix();   
      }
   }
   
   // Draw the solid.
   if(draw_solid) {
      glDisable(GL_LIGHTING);
      glColor3f(0.7, 0.6, 0.2);
      glLineWidth(1);
      glBegin(GL_LINES);
      for(int i = 0; i < tris.size(); i++) {
         for(int j = 0; j < 3; j++) {
            glVertex3fv(vertices[tris[i][j]].v);
            glVertex3fv(vertices[tris[i][(j + 1) % 3]].v);
         }
      }
      glEnd();
   }
 
   //Draw the bound box for good measure
   glDisable(GL_LIGHTING);
   glColor3f(0,0,0);
   glLineWidth(2);
   glBegin(GL_LINES);
   glVertex3f(0,0,0);
   glVertex3f(0,0,1);

   glVertex3f(0,0,0);
   glVertex3f(0,1,0);

   glVertex3f(0,0,0);
   glVertex3f(1,0,0);

   glVertex3f(0,1,0);
   glVertex3f(1,1,0);

   glVertex3f(1,1,0);
   glVertex3f(1,0,0);

   glVertex3f(1,0,0);
   glVertex3f(1,0,1);

   glVertex3f(0,1,0);
   glVertex3f(0,1,1);

   glVertex3f(1,1,0);
   glVertex3f(1,1,1);

   glVertex3f(0,1,1);
   glVertex3f(1,1,1);

   glVertex3f(1,0,1);
   glVertex3f(1,1,1);

   glVertex3f(0,0,1);
   glVertex3f(1,0,1);

   glVertex3f(0,0,1);
   glVertex3f(0,1,1);

   glEnd();
}

struct ScreenShotButton : public Gluvi::Button {
   const char *filename_format;
   ScreenShotButton(const char *label, const char *filename_format_) : Gluvi::Button(label), filename_format(filename_format_) {}
   void action()
   { 
      Gluvi::ppm_screenshot(filename_format, frame); 
   }
};

struct MovieButton : public Gluvi::Button {
   const char *filename_format;
   MovieButton(const char *label, const char *filename_format_) : Gluvi::Button(label), filename_format(filename_format_) {}
   void action()
   { 
      if(!running) {
         if(!filming) {
            filming = true;
            glutTimerFunc(1000, timer, 0);
         }
         else {
            filming = false;
         }
      }
   }
};

struct RunButton : public Gluvi::Button {
   RunButton(const char *label) : Gluvi::Button(label){}
   void action()
   { 
      if(!filming) {
         if(!running) {
            running = true;
            glutTimerFunc(1000, timer, 0);
         }
         else {
            running = false;
         }
      }
   }
};

void keyPress(unsigned char key, int x, int y) {

   if(key == 'r') {
      if(!filming) {
         if(!running) {
            running = true;
            glutTimerFunc(1000, timer, 0);
         }
         else {
            running = false;
         }
      }
   }
   else if(key == 'f') {
      if(!running) {
         if(!filming) {
            filming = true;
         }
         else {
            filming = false;
         }
      }
   }
   glutPostRedisplay();



}

void special_key_handler(int key, int x, int y)
{
   int mods=glutGetModifiers();
   switch(key){
case GLUT_KEY_LEFT:
   if(mods == GLUT_ACTIVE_SHIFT) {
      if(read_frame(0))
         glutPostRedisplay();
   }
   else {
      if(read_frame(frame-1))
         glutPostRedisplay();
   }
   break;
case GLUT_KEY_RIGHT:
   if(mods == GLUT_ACTIVE_SHIFT) {
      if(read_frame(highest_frame))
         glutPostRedisplay();
   }
   else {
      if(read_frame(frame+1))
         glutPostRedisplay();
   }
   break;
default:
   ;
   }
}


Gluvi::Target3D* cam_local;
int main(int argc, char **argv)
{
   Gluvi::init("Liquid Data Viewer", &argc, argv);
   file_path = "odata/";
   
   if(!get_tris() || !read_frame(frame)) {
      return 1;
   }
   
   glutSpecialFunc(special_key_handler);
   glutKeyboardFunc(keyPress);
   
   Gluvi::Target3D cam;
   set_view(cam);
   Gluvi::camera = &cam;
   cam_local = &cam;
   
   Gluvi::userDisplayFunc = display;
   
   Gluvi::StaticText frametext(frame_number.c_str());
   Gluvi::root.list.push_back(&frametext);

   ppmfileformat = new char[strlen(file_path.c_str())+100];
   sprintf(ppmfileformat, "img/screenshot%%04d.ppm");

   ScreenShotButton screenshot("Screenshot", ppmfileformat);
   Gluvi::root.list.push_back(&screenshot);

   MovieButton movie("Movie", ppmfileformat);
   Gluvi::root.list.push_back(&movie);

   RunButton run("Run");
   Gluvi::root.list.push_back(&run);
   
   non_inter = argc > 1;
   if(non_inter) {
      movie.action();
   }

   Gluvi::run();
   return 0;
}
