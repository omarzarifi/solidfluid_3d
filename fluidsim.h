#ifndef FLUIDSIM_H
#define FLUIDSIM_H

#include <fstream>

#include "deformable.h"
#include "types.h"

#include "Eigen/Sparse"

const int PPM_DECOUPLED = 1;
const int PPM_FORCE_JACOBIAN = 2;
const int PPM_FORCE_JACOBIAN_SPD = 3;
const int PPM_FORCE_JACOBIAN_SPD_DYN = 4;
const int PPM_FORCE_JACOBIAN_DAMPING = 5;
const int PPM_FORCE_JACOBIAN_SPD_FAC = 6;
const int PPM_FORCE_JACOBIAN_SPD_FAC2 = 7;

class Deformable;
class BoundarySample;

/**
 * This class represents a fluid that is able to interact with solid objects.
 */
class Fluid {

public:
   
   /**
    * Default constructor does nothing.
    */
   Fluid();
   
   /**
    * The "constructor" initializes internal data structures. The domain is set
    * to be 1x1. pr_ is particle radius.
    */
   void initialize(fptype rho_, int ni_, fptype pr_);

   /**
    * Advance the simulation by the given time step.
    */
   void advance(fptype dt);
   
   /**
    * Adds a simulation particle to the fluid.
    */
   void addParticle(const Vec3fp& position);
   
   /**
    * Sets the acceleration due to gravity.
    */
   void setGravity(fptype gx, fptype gy, fptype gz);
   
   /**
    * Sets the deformable that will interact with the fluid.
    */
   void setDeformable(Deformable* def_);
   
   /**
    * Applies the given acceleration uniformly to the fluid.
    */
   void addAcceleration(fptype a[3], fptype dt);
   
   /**
    * Applies the given acceleration to the fluid above the given y
    * coordinate.
    */
   void addAccelerationAbove(fptype a[3], fptype dt, fptype y);
   
   /**
    * Interpolates fluid velocity from the grid values.
    */
   Vec3fp getVelocity(const Vec3fp& position) const;
   
   /**
    * Renders the fluid.
    */
   void render() const;
   
   /**
    * Set the method to use for pressure projection.
    */
   void setProjectionMethod(int method);
   
   /**
    * If true is passed, an iterative method is used to solve the system.
    * Otherwise, linear systems are solved directly.
    */
   void setIterativeSolve(bool a);
   
   /**
    * Can be used to turn on or off time step size limiting via the CFL
    * condition.
    */
   void setUseCFL(bool a);
   
   /**
    * Can be used to turn on matrix exports.
    */
   void writeToMatlab(bool a);
   
   /**
    * Sets the output stream to the file whose name is given.
    */
   bool setInfoStream(const char* file);
   
   /**
    * Adds a spherical blob of fluid at the given position.
    */
   void addSphere(const Vec3fp& c, float r, int seed = 0, int perCell = 8);
   
   // Grid resolution.
   int ni;
   fptype dx;
   // Fluid density.
   fptype rho;
   
   // The deformable body to interact with.
   Deformable* def;
   // Vector of solid boundary segments intersected with the grid.
   std::vector<BoundarySample> boundary;
   
   // Force of gravity.
   fptype g[3];
   
   // Fluid velocity.
   Array3fp us, temp_us;      // Vertical velocities...
   Array3fp vs, temp_vs;      // Horizontal velocities...
   Array3fp ws, temp_ws;      // Depth velocities...
   
   // List of particles.
   std::vector<Vec3fp> particles;
   // Size of each particle.
   fptype particle_radius;
   
   // List of regular pressure sample indices.
   Array3i F;
   // Arrays of fluid face areas.
   Array3fp Fu, Fv, Fw;
   
   // Arrays for signed distance field values in the grid cells and nodes.
   Array3fp liquid_phi;
   Array3fp solid_phi;
   
   // Method to use for projection.
   int projection_method = PPM_FORCE_JACOBIAN;
   // How to solve the linear system (direct or iterative).
   bool iterative_solve = false;
   // Whether or not use the CFL condition to limit maximum time step.
   bool use_CFL = true;
   
   // Whether or not to apply gravity to the solid.
   bool solid_gravity = true;
   
   // Output stream to which information is sent.
   std::ofstream info_stream;
   // Whether or not to write out the matrix system to file.
   bool export_mat = false;
   // Internal counter of time step.
   int timestep_c = 0;
   
   // Coefficient of viscosity.
   //fptype viscosity_mu = 0;

protected:
   
   /**
    * Find the largest timestep that satisfies the CFL condition.
    */
   fptype cfl();
   
   /**
    * Moves the particles with their current velocities.
    */
   void advectParticles(fptype dt);
   
   /**
    * Use RK2 to advect around the velocity field.
    */
   Vec3fp traceRK2(Vec3fp &position, fptype dt);
   
   /**
    * Performs advection of the velocity field.
    */
   void advect(fptype dt);
   
   /**
    * Explicitly applies viscosity.
    */
   //void applyViscosity(fptype dt);
   
   /**
    * Initializes the matrix of pressure indices.
    */
   int setF();
   
   /**
    * Sets the matrices of horizontal and vertical fluid face areas.
    */
   void setFAreas();
   
   /**
    * Use the particles to compute the signed distance field.
    */
   void computePhi();
   
   /**
    * Given cells (i, j, k) and (i + i_off, j + j_off, k + k_off), this
    * function computes the fraction of the segment connecting their centres
    * that is inside the fluid.
    */
   fptype getTheta(int i, int j, int k, int i_off, int j_off, int k_off);
   
   /**
    * Performs pressure projection.
    */
   void pProject(fptype dt);
   
   /**
    * First pressure projection scheme. Uses force Jacobian, as in the
    * indefinite formulation for rigid bodies.
    */
   void pProject_v1(fptype dt);
   
   /**
    * Second pressure projection scheme. Uses force Jacobian, but results in a
    * symmetric positive definite system.
    */
   void pProject_v2(fptype dt);
   
   /**
    * Third pressure projection scheme. Uses force Jacobian, but results in a
    * symmetric positive definite system. This is the same as v2, except the
    * K_MULTIPLIER is dynamically computed every time step.
    */
   void pProject_v2b(fptype dt);
   
   /**
    * Fourth pressure projection scheme. Performs two decoupled solves (one for
    * the fluid, the other for the solid).
    */
   void pProject_v3(fptype dt);
   
   /**
    * Fifth pressure projection scheme. Performs a decoupled elastic solve for
    * the solid, followed by a coupled damping solve (without casting to SPD
    * form).
    */
   void pProject_v4(fptype dt);
   
   /**
    * Sixth projection scheme; performs coupled SPD projection, while factoring
    * the solid stiffness matrix.
    */
   void pProject_v5(fptype dt);
   
   /**
    * Seventh projection scheme; performs coupled SPD projection, while
    * factoring the solid stiffness matrix (introducing "stress" variables).
    */
   void pProject_v6(fptype dt);
   
   /**
    * This function interpolates pressure in an air cell from its valid
    * neighbours.
    */
   fptype getOPressure(const VectorXfp& p, int i, int j, int k);
   
   /**
    * This function sets the valid locations of velocity samples.
    */
   void setValid();
   
   /**
    * This function constrains the normal component of the velocity field
    * inside the solid to match the solid's movement.
    */
   void constrainVelocities();

};

#endif
