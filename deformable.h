#ifndef DEFORMABLE_H
#define DEFORMABLE_H

#include "fluidsim.h"
#include "types.h"

#include "Eigen/Dense"

class Fluid;

/**
 * This class represents a boundary segment.
 */
class BoundarySample {
   
public:
   
   /**
    * This constructor initializes the object's members to the input values.
    */
   BoundarySample(fptype x_, fptype y_, fptype z_, fptype nx_, fptype ny_,
      fptype nz_, int n_index1_, int n_index2_, int n_index3_, fptype barc1_,
      fptype barc2_, fptype barc3_, int p_ind_);
   
   // Components of the inner-pointing area-weighted normal to the segment.
   fptype n[3];
   
   // Indices of the solid nodes to which the segment belongs.
   int n_index[3];
   
   // Barycentric coordinate of the centre with respect to the nodes.
   fptype barc[3];
   
   // Pressure index of the cell containing this segment.
   int p_ind;
   
};

/**
 * A deformable body. This is an abstract class that facilitates the use of
 * finite element models.
 */
class Deformable {

public:
   
   /**
    * Sets the shape of this deformable to be rectangular with the given
    * dimensions. The deformable must still be built after this stage. The last
    * two arguments specify whether or not to constrain the nodes at x = 0 and
    * x = max.
    */
   void createCube(fptype dx, int nxh, int nyh, int nzh,
      bool constrainXL = false, bool constrainXR = false,
      bool constrainYL = false, bool constrainYR = false);
   
   /**
    * Sets the shape of this deformable to be rectangular with the given
    * dimensions. The deformable must still be built after this stage. The last
    * two arguments specify whether or not to constrain the nodes at x = 0 and
    * x = max.
    * Unlike createCube(), this method does not flip elements.
    */
   void createCubeAsym(fptype dx, int nx, int ny, int nz,
      bool constrainXL = false, bool constrainXR = false,
      bool constrainYL = false, bool constrainYR = false);
   
   /**
    * Sets the shape of this deformable by reading the given file.
    */
   void createFromFile(const char* file, double scale = 1);
   
   /**
    * Builds the system for the deformable body, with the given material Lame
    * parameters and time step, using the current vertex configuration for rest
    * state. dm and ds are coefficients for Rayleigh damping.
    */
   virtual void build(fptype rho, fptype mu, fptype lambda, fptype dm,
      fptype ds) = 0;
   
   /**
    * Shifts the coordinates of every node by the given offsets.
    */
   void shift(fptype x, fptype y, fptype z);
   
   /**
    * Rotates the solid by the given angles with respect to the origin.
    */
   void rotate(fptype angleX, fptype angleY, fptype angleZ);
   
   /**
    * Scales the solid using the given factors.
    */
   void scale(fptype sx, fptype sy, fptype sz);
   
   /**
    * Moves the solid using its current nodal velocities.
    */
   void move(fptype dt);
   
   /**
    * Assembles the stiffness matrix for the solid using its current
    * configuration and stores it in B. Also computes the current damping matrix
    * and shift vector. This function should be called before stepping.
    */
   virtual void assembleStiffness() = 0;
   
   /**
    * Assembles the factored stiffness matrix for the solid using its current
    * configuration and stores it in C. Also computes the shift vector.
    */
   virtual void assembleFactoredStiffness() = 0;
   
   /**
    * Integrates elastic and damping forces with the given time step.
    */
   void step(fptype dt);
   
   /**
    * Integrates elastic forces with the given time step (ignores damping).
    */
   void stepE(fptype dt);
   
   /**
    * Applies a uniform acceleration field to the solid with the given time
    * step.
    */
   void addAcceleration(fptype a[3], fptype dt);
   
   /**
    * Computes the signed distance function with respect to this solid on the
    * nodal grid whose bottom left corner is the origin, with grid spacing of
    * dx.
    */
   void computeSolidPhi(fptype dx, Array3fp& nodal_phi);
   
   /**
    * Intersects this solid with the cubic 0-1 grid, where the size of each
    * cell is dx. Returns a vector of segments, each of which is contained in a
    * single cell. A segment is added only if its cell is active (as determined
    * from F).
    */
   void intersectGrid(std::vector<BoundarySample>& b, fptype dx,
      const Array3i& F, fptype minArea = 0);
   
   /**
    * Projects out the normal component of velocity for samples in v that are
    * valid and inside the object. phi is the nodal signed distance field with
    * respect to the solid. dir denotes which velocity components are stored in
    * v (0 is x, 1 is y, 2 is z).
    */
   void projectNormalV(fptype dx, Array3fp& v, const Array3fp& valid,
      const Array3fp& phi, const Fluid& f, int dir);
   
   /**
    * Gets the velocity of the particle in the given location.
    */
   Vec3fp getVelocity(const Vec3fp& location);
   
   /**
    * Sets the domain of the deformable body.
    */
   void setBounds(fptype x0, fptype x1, fptype y0, fptype y1, fptype z0,
      fptype z1);
   
   /**
    * Returns the maximum speed along an axis of the vertices of this solid.
    */
   fptype maxV();
   
   /**
    * Returns the total volume of this solid.
    */
   fptype getVolume();
   
   /**
    * Renders this body.
    */
   void render();
   
   VectorXfp xs;           // Vector of all vertex coordinates.
   VectorXfp vs;           // Vector of unconstrained vertex velocities.
   int* elements;          // Vertex connectivity graph.
   int* boundary;          // List of boundary triangles.
   int n_v;                // Number of vertices.
   int n_t;                // Number of tetrahedra.
   int n_b;                // Number of boundary triangles.
   fptype xbounds[2];      // Horizontal bounding area.
   fptype ybounds[2];      // Vertical bounding area.
   fptype zbounds[2];      // Depth bounding area.
   
   // Number of constrained nodes.
   int n_constrained;
   // Indices of constrained nodes.
   int* constrainedI;
   // Mapping from original indices to indices in the reduced system.
   // Constrained nodes are mapped to -1.
   int* constrained;
   // Reduced position and velocity vectors.
   VectorXfp xs2;
   VectorXfp vs2;
   
   // Mass matrix for the solid.
   SparseMatrixXfp M;
   // Inverse of the mass matrix for the solid.
   SparseMatrixXfp Minv;
   // Stiffness matrix for the solid.
   SparseMatrixXfp B;
   // Factored stiffness matrix for the solid.
   SparseMatrixXfp C;
   // Damping matrix for the solid.
   SparseMatrixXfp D;
   // Right hand side shift vector.
   VectorXfp v;
   
   // Coefficients for Rayleigh damping.
   fptype d_m;
   fptype d_s;
   
protected:
   
   // Bounding box sides.
   fptype bb_minx;
   fptype bb_maxx;
   fptype bb_miny;
   fptype bb_maxy;
   fptype bb_minz;
   fptype bb_maxz;
   
};

/**
 * This concrete subclass of Deformable implements a corotated linear elasticity
 * model.
 */
class CorotatedLinearDeformable : public Deformable {
  
public:
   
   /**
    * Creates an empty deformable body that needs to be set to a shape and
    * built.
    */
   CorotatedLinearDeformable();
   
   /**
    * Builds the system for the deformable body, with the given material Lame
    * parameters and time step, using the current vertex configuration for rest
    * state. dm and ds are coefficients for Rayleigh damping.
    */
   void build(fptype rho, fptype mu, fptype lambda, fptype dm, fptype ds);
   
   /**
    * Assembles the stiffness matrix for the solid using its current
    * configuration and stores it in B. Also computes the current damping matrix
    * and shift vector. This function should be called before stepping.
    */
   void assembleStiffness();
   
   /**
    * Assembles the factored stiffness matrix for the solid using its current
    * configuration and stores it in C. Also computes the shift vector.
    */
   void assembleFactoredStiffness();
   
protected:
   
   // Lists of local stiffness matrices and shift vectors.
   std::vector<Matrix12fp> localBs;
   std::vector<Vector12fp> localvs;
   std::vector<Matrix6x12fp> localBsFactored;
   // List of inverse barycentric matrices.
   std::vector<Matrix4fp> Pinvs;
   
};

#endif
