#ifndef TYPES_H
#define TYPES_H

#include "array3.h"
#include "vec.h"

#include "Eigen/Sparse"

typedef double fptype;

typedef Eigen::SparseMatrix<fptype> SparseMatrixXfp;
typedef Eigen::Matrix<fptype, Eigen::Dynamic, 1> VectorXfp;

typedef Eigen::Matrix<fptype, 2, 1> Vector2fp;
typedef Eigen::Matrix<fptype, 3, 1> Vector3fp;
typedef Eigen::Matrix<fptype, 9, 1> Vector9fp;
typedef Eigen::Matrix<fptype, 12, 1> Vector12fp;

typedef Eigen::Matrix<fptype, 3, 2> Matrix3x2fp;
typedef Eigen::Matrix<fptype, 3, 3> Matrix3fp;
typedef Eigen::Matrix<fptype, 4, 4> Matrix4fp;
typedef Eigen::Matrix<fptype, 6, 9> Matrix6x9fp;
typedef Eigen::Matrix<fptype, 6, 12> Matrix6x12fp;
typedef Eigen::Matrix<fptype, 9, 9> Matrix9fp;
typedef Eigen::Matrix<fptype, 12, 12> Matrix12fp;

typedef Array3<fptype, Array1<fptype> > Array3fp;

typedef Vec<3, fptype> Vec3fp;

#endif
