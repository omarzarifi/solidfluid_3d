#include <cstdio>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cfloat>

#include "deformable.h"
#include "fluidsim.h"
#include "gluvi.h"
#include "openglutils.h"

using namespace std;

// Display properties.
bool draw_grid = true;
bool draw_velocities = false;
bool draw_fluid = true;
bool draw_solid = true;

double timestep = 1.0 / 60;
double g[] = {0, -3};

Deformable* def;
double defMass;
Fluid fluid;

// Gluvi stuff.
Gluvi::PanZoom2D cam(-0.1f, -0.35f, 1.2f);
double oldmousetime;
Vec2f oldmouse;
void display();
void mouse(int button, int state, int x, int y);
void drag(int x, int y);
void timer(int junk);

static bool drawFrames;
static float simTime = 0;
static float nextFrameTime = 0;
static int frameNumber;
static const float FRAME_TIME = timestep;

//Main testing code
//-------------
int main(int argc, char **argv)
{
   // Set up viewer stuff.
   Gluvi::init("Solid Fluid Coupling", &argc, argv);
   Gluvi::camera=&cam;
   Gluvi::userDisplayFunc=display;
   Gluvi::userMouseFunc=mouse;
   Gluvi::userDragFunc=drag;
   glClearColor(1, 1, 1, 1);
   
   glutTimerFunc(1000, timer, 0);
   
   // Solid material parameters.
   double rho = 1;            // Density.
   double mu = 1000000;       // Second Lame parameter.
   double lambda = 1000000;   // First Lame parameter.
   double damping_m = 0;      // Mass damping coefficient.
   double damping_s = 0.05;   // Stiffness damping coefficient.
   
   // Fluid parameters.
   int N = 32;                // Grid resolution.
   int M = 5;                 // Particles per grid cell per dimension.
   double dx = 1.0 / N;       // Grid cell size.
   double dx2 = dx / M;       // Particle separation.
   double rad = 2.5 / N / M;  // Particle radius.
   
   // Solid shift amounts.
   double sx = dx * 13.5;
   double sy = dx * 8.5;
   // Solid shape parameters.
   double sdx = sqrt(2.0) * dx;
   int nx = 6;
   int ny = 4;
   // Set up the solid.
   def = new CorotatedLinearDeformable();
   def->createSymRectangle(sdx, nx / 2, ny / 2);
   def->build(rho, mu, lambda, damping_m, damping_s);
   defMass = def->getVolume() * rho;
   def->rotate(-0.785398163397448);
   def->shift(sx, sy);
   
   // Define the region not occupied by the fluid (due to the submerged solid).
   double sx0 = sx - rad / 2;
   double sx1 = sx + sdx * nx + rad / 2;
   double sy0 = sy - rad / 2;
   double sy1 = sy + sdx * ny + rad / 2;
   // For fluid simulation.
   fluid.initialize(1, N, rad);
   fluid.setProjectionMethod(PPM_FORCE_JACOBIAN);
   fluid.setGravity(g[0], g[1]);
   fluid.setDeformable(def);
   for(int i = 1; i < N - 1; i++) {
      for(int j = 1; j < N / 2 + 2; j++) {
         for(int k1 = 0; k1 < M; k1++) {
            for(int k2 = 0; k2 < M; k2++) {
               Vec2d pos(i * dx + (k1 + 0.5) * dx2, j * dx + (k2 + 0.5) * dx2);
               //if(!(pos[0] > sx0 && pos[0] < sx1 && pos[1] > sy0
                  //&& pos[1] < sy1)) {
                  fluid.addParticle(pos);
               //}
            }
         }
      }
   }
   
   // Give the fluid and solid some initial velocities.
   double accel[] = {0, 0};
   fluid.addAcceleration(accel, 1);
   def->addAcceleration(accel, 1);
   
   def->setBounds(dx, 1 - dx, dx, 1 - dx);
   
   drawFrames = argc > 1;

   Gluvi::run();

   return 0;
}


void display(void)
{
   // Draw the grid.
   if(draw_grid) {
      glColor3f(0, 0, 0);
      glLineWidth(1);
      draw_grid2d(Vec2f(0, 0), fluid.dx, fluid.ni, fluid.ni);
   }
      
   // Render the fluid.
   if(draw_fluid) {
      fluid.render();
   }
   
   // Render the solid.
   if(draw_solid) {
      glColor3f(1, 0, 0);
      glLineWidth(2);
      def->render();
   }
   
   // Draw the velocities
   if(draw_velocities) {
      glColor3f(1, 0, 0);
      for(int i = 0; i < fluid.ni; i++) {
         for(int j = 0; j < fluid.ni; j++) {
            Vec2d pos((i + 0.5) * fluid.dx,(j + 0.5) * fluid.dx);
            draw_arrow2d(pos, pos + fluid.getVelocity(pos),
               0.2 * fluid.dx);
         }
      }
   }
}

void mouse(int button, int state, int x, int y)
{
   Vec2f newmouse;
   cam.transform_mouse(x, y, newmouse.v);

   oldmouse=newmouse;
}

void drag(int x, int y)
{
   Vec2f newmouse;
   cam.transform_mouse(x, y, newmouse.v);

   oldmouse=newmouse;
}

void timer(int junk)
{
   if(drawFrames && simTime >= nextFrameTime) {
      Gluvi::ppm_screenshot("img/frame%04d.ppm", frameNumber++);
      nextFrameTime += FRAME_TIME;
   }
   
   // Advance the coupled simulation.
   fluid.advance(timestep);
   
   simTime += timestep;
   if(std::floor(simTime) > simTime - timestep) {
      double v = def->getVolume();
      std::cout << "Simulation time: " << simTime << "s\t\tsolid volume: " <<
         v << "\t\tsolid density: " << defMass / v << std::endl;
   }

   glutPostRedisplay();
   glutTimerFunc(timestep, timer, 0);
}
