#include <cstdio>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cfloat>

#include "fluidsim.h"
#include "gluvi.h"

using namespace std;

fptype timestep = 1.0 / 60;
fptype g[] = {0, -4, 0};

Fluid fluid;

// Gluvi stuff.
float target[] = {0.5, 0.5, 0.5};
Gluvi::Target3D cam(target, 1.5f, 0.0f, -1.0f, 60.0f, 0.01f, 100.0f);
double oldmousetime;
Vec2f oldmouse;
void display();
void mouse(int button, int state, int x, int y);
void drag(int x, int y);
void timer(int junk);

static bool drawFrames;
static float simTime = 0;
static float nextFrameTime = 0;
static int frameNumber;
static const float FRAME_TIME = timestep;

/**
 * Creates a simple dam break simulation.
 */
int main(int argc, char **argv)
{
   // Set up viewer stuff.
   Gluvi::init("Solid Fluid Coupling", &argc, argv);
   Gluvi::camera=&cam;
   Gluvi::userDisplayFunc=display;
   Gluvi::userMouseFunc=mouse;
   Gluvi::userDragFunc=drag;
   glClearColor(1, 1, 1, 1);
   
   glutTimerFunc(1000, timer, 0);
   
   // Fluid parameters.
   int N = 22;                // Grid resolution.
   int M = 2;                 // Particles per grid cell per dimension.
   double dx = 1.0 / N;       // Grid cell size.
   double dx2 = dx / M;       // Particle separation.
   double rad = dx * 1.01 * sqrt(3.0) / 2.0;
      //2.5 / N / M;  // Particle radius.
   
   // Set up the fluid.
   fluid.initialize(1, N, rad);
   fluid.setIterativeSolve(true);
   fluid.setGravity(g[0], g[1], g[2]);
   for(int i = 1; i < N / 2; i++) {
      for(int j = 1; j < N - 1; j++) {
         for(int k = 1; k < N / 2; k++) {
            for(int k1 = 0; k1 < M; k1++) {
               for(int k2 = 0; k2 < M; k2++) {
                  for(int k3 = 0; k3 < M; k3++) {
                     Vec3fp pos(i * dx + (k1 + 0.5) * dx2,
                        j * dx + (k2 + 0.5) * dx2, k * dx + (k3 + 0.5) * dx2);
                     fluid.addParticle(pos);
                  }
               }
            }
         }
      }
   }
   
   drawFrames = argc > 1;

   Gluvi::run();

   return 0;
}


void display(void)
{
   fluid.render();
}

void mouse(int button, int state, int x, int y)
{
   Vec2f newmouse;
   //cam.transform_mouse(x, y, newmouse.v);

   oldmouse=newmouse;
}

void drag(int x, int y)
{
   Vec2f newmouse;
   //cam.transform_mouse(x, y, newmouse.v);

   oldmouse=newmouse;
}

void timer(int junk)
{
   if(drawFrames && simTime >= nextFrameTime) {
      Gluvi::ppm_screenshot("img/frame%04d.ppm", frameNumber++);
      nextFrameTime += FRAME_TIME;
   }
   
   // Advance the fluid.
   fluid.advance(timestep);
   
   simTime += timestep;
   if(std::floor(simTime) > simTime - timestep) {
      std::cout << "Simulation time: " << simTime << ".\n";
   }
   
   glutPostRedisplay();
   glutTimerFunc(timestep, timer, 0);
}
