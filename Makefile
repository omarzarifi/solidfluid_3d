ifndef config
	config=release
endif
export config

ifeq ($(config),debug)
	CPPFLAGS = -g -fopenmp -std=c++0x
endif

ifeq ($(config),release)
	CPPFLAGS = -O3 -fopenmp -std=c++0x
endif

ARRAY1_H = array1.h
UTIL_H = util.h

ARRAY2_H = array2.h $(ARRAY1_H)
ARRAY3_H = array3.h $(ARRAY1_H)
VEC_H = vec.h $(UTIL_H)

ARRAY3UTILS_H = array3_utils.h $(VEC_H) $(ARRAY3_H) $(UTIL_H)
GLUVI_H = gluvi.h $(VEC_H)
TYPES_H = types.h $(ARRAY3_H) $(VEC_H)

DEFORMABLE_H = deformable.h fluidsim.h $(ARRAY3UTILS_H) $(TYPES_H)
MAKELEVELSET3_H = makelevelset3.h $(TYPES_H)
OPENGLUTILS_H = openglutils.h $(VEC_H) $(ARRAY2_H) $(TYPES_H)

FLUIDSIM_H = fluidsim.h $(DEFORMABLE_H) $(TYPES_H)

all: app appC appD appF viewer

app: deformable.o fluidsim.o gluvi.o main2.o makelevelset3.o openglutils.o
	g++ -fopenmp deformable.o fluidsim.o gluvi.o main2.o makelevelset3.o \
		openglutils.o -o app -lGL -lglut -lGLU

appC: deformable.o fluidsim.o gluvi.o mainTest.o makelevelset3.o openglutils.o
	g++ -fopenmp deformable.o fluidsim.o gluvi.o mainTest.o \
		makelevelset3.o openglutils.o -o appC -lGL -lglut -lGLU

appD: deformable.o fluidsim.o gluvi.o mainDef.o makelevelset3.o openglutils.o
	g++ -fopenmp deformable.o fluidsim.o gluvi.o mainDef.o \
		makelevelset3.o openglutils.o -o appD -lGL -lglut -lGLU

appF: deformable.o fluidsim.o gluvi.o mainFluid.o makelevelset3.o openglutils.o
	g++ -fopenmp deformable.o fluidsim.o gluvi.o mainFluid.o \
		makelevelset3.o openglutils.o -o appF -lGL -lglut -lGLU

viewer: gluvi.o openglutils.o viewer.o
	g++ gluvi.o openglutils.o viewer.o -o viewer -lGL -lglut -lGLU

deformable.o: deformable.cpp $(DEFORMABLE_H) $(MAKELEVELSET3_H) $(UTIL_H)
	g++ $(CPPFLAGS) -c deformable.cpp -o deformable.o

fluidsim.o: fluidsim.cpp $(ARRAY3UTILS_H) $(FLUIDSIM_H) $(OPENGLUTILS_H)
	g++ $(CPPFLAGS) -c fluidsim.cpp -o fluidsim.o

gluvi.o: gluvi.cpp $(GLUVI_H) $(VEC_H)
	g++ $(CPPFLAGS) -c gluvi.cpp -o gluvi.o

main2.o: main2.cpp $(DEFORMABLE_H) $(FLUIDSIM_H)
	g++ $(CPPFLAGS) -c main2.cpp -o main2.o

mainDef.o: mainDef.cpp $(DEFORMABLE_H) $(GLUVI_H)
	g++ $(CPPFLAGS) -c mainDef.cpp -o mainDef.o

mainFluid.o: mainFluid.cpp $(FLUIDSIM_H) $(GLUVI_H)
	g++ $(CPPFLAGS) -c mainFluid.cpp -o mainFluid.o

mainTest.o: mainTest.cpp $(DEFORMABLE_H) $(FLUIDSIM_H)
	g++ $(CPPFLAGS) -c mainTest.cpp -o mainTest.o

makelevelset3.o: makelevelset3.cpp $(MAKELEVELSET3_H) $(VEC_H)
	g++ $(CPPFLAGS) -c makelevelset3.cpp -o makelevelset3.o

openglutils.o: openglutils.cpp $(OPENGLUTILS_H) $(VEC_H)
	g++ $(CPPFLAGS) -c openglutils.cpp -o openglutils.o

viewer.o: viewer.cpp $(GLUVI_H) $(OPENGLUTILS_H)
	g++ $(CPPFLAGS) -c viewer.cpp -o viewer.o

.PHONY: clean
clean:
	rm -f deformable.o fluidsim.o gluvi.o main2.o mainDef.o mainFluid.o \
		mainTest.o makelevelset3.o openglutils.o viewer.o app appC \
		appD appF viewer
